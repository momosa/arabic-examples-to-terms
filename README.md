# Living Arabic Project: Examples to Terms

This system was developed as a part of
[The Living Arabic Project][living_arabic].

## Overview

This is a proof of concept for taking sentences in various Arabic dialects
and determining corresponding terms in a given dictionary.
The given sentences are considered good examples of the selected terms.

In the following,
the sentence ﻱﺍ ﺐَﺷَﺭ ﺶُﻔْﺗُﻭﺍ ﺎﻠﻘُﻣَﺭ (ﺎﻠﮕُﻣَﺭ) (People, have you seen the moon?)
is a good example of the terms ﺐﺷﺭ (people) and ﻖﻣﺭ (moon).

```
يا بَشَر شُفْتُوا القُمَر (الگُمَر) 
 People, have you seen the moon?
Target term: بشر
Target definition: mankind, man, men, people (typically definite, as in البَشَر)
      Agg Weight     Term Weight  Meaning Weight     Token
        0.390008        0.202772        1.000000       بشر
        0.345528        0.315597        0.750000       قمر
        0.183399        0.315597        0.250000       قمر
        0.081064        0.000000        0.250000        يا
           TokenDefinition
             بشرmankind, man, men, people (typically definite, as in البَشَر)
             قمرmoon
             قمر(reference to) beautiful person, beauty (can be said of a man or woman)
              ياoh, O, hey (vocative and exclamatory particle)
```

### Results and limitations

This proof of concept determines word-and-meaning pairs
from each given example.

The results are quite accurate and
do a good job of supplementing human input.

The primary limitation is word recognition based on tokenization.
Known cases include those in which the dictionary term:

1. contains non-classical characters, like ﭺ, or
1. is composed mostly of weak characters.

### Approach

The basic idea for choosing terms comes from attention-based models.
In particular, we use
[AraBERT][arabert_github],
based on Google's BERT.
BERT outputs twelve attention layers
indicating relationships between words.
It's been observed ([Clark et al. 2019][clark2019]) that
different layers tend to favor different types of terms,
such as nouns or verbs.

Our hypothesis was that some linear combination of the twelve attention layers
could be used to detect which words in a sentence are the most salient,
and furthermore reproduce subtle patterns by which humans manually select
the terms that sentences are examples of.
We trained a system to determine those twelve weights,
one for each layer.

Our little AI gave us more than a 30% increase in selecting the same term as a human.
Furthermore, the correct term was usually one of the top three.
These results provide good steps in the right direction,
especially considering that a given example can be of more than one term.

The preceding can't account for synonyms.
For instance, notice that ﻖﻣﺭ appears twice
as a candidate solution in the following:

```
يا بَشَر شُفْتُوا القُمَر (الگُمَر) 
 People, have you seen the moon?
Target term: بشر
Target definition: mankind, man, men, people (typically definite, as in البَشَر)
      Agg Weight     Term Weight  Meaning Weight     Token
        0.390008        0.202772        1.000000       بشر
        0.345528        0.315597        0.750000       قمر
        0.183399        0.315597        0.250000       قمر
        0.081064        0.000000        0.250000        يا
           TokenDefinition
             بشرmankind, man, men, people (typically definite, as in البَشَر)
             قمرmoon
             قمر(reference to) beautiful person, beauty (can be said of a man or woman)
              ياoh, O, hey (vocative and exclamatory particle)
```

The AI identifies قمر as a one good candidate (as well as بشر),
as the "Term Weight" column shows.
But the term weight doesn't determine which meaning of قمر best matches the example.
So we compare the English definitions to the English example
to determine the best meaning of قمر for the example sentence.
That comparison is represented in the "Meaning Weight" column.

Finally, we combine the term and meaning weights to get the aggregate weight.
We can see from the "Agg Weight" column that ﺐﺷﺭ (people) and ﻖﻣﺭ (moon)
are the preferred terms, while ﻖﻣﺭ (beautiful person) scores significantly lower.

### Next steps

Next steps for the Living Arabic Project:

* Let's use it!

Some next steps for improving results of the proof of concept:

1. Improve term recognition.
   This could be a rather large endeavor since it requires delving into the tokenizer.
1. Branch off of AraBERT to improve attention analysis.
   (Prereq: improved term recognition.)
1. Handle English synonyms (could just use a thesaurus).
1. Enhance meaning analysis, possibly training an AI.
1. Integrate a part-of-speech analysis.

## Technical setup

These instructions utilize `pyenv` to manage the virtual environment.

Start by installing Python and `pyenv`.

Install the correct version of Python:

```shell
pyenv install 3.9.7
```

Create the virtual environment from the project directory:

```shell
pyenv virtualenv 3.9.7 living-arabic-examples-to-terms
```

Now you can activate the virtual environment:

```shell
pyenv activate living-arabic-examples-to-terms
```

With the virtual environment activated,
install the dependencies:

```shell
pip install -r requirements-dev.txt
```

Finally, install JAVA.
It's required for the Farasa tokenizer.

## Usage

Once the dependencies are installed,
you can use the scripts provided
to process data.
All scripts have a `--help` option and
create log files in the project directory.

Default configuration options for scripts,
such as log level or epochs,
are located in `config.json`.
Options can be adjusted when running scripts, as well.
See the `--help` for each script for available options.

### Standardizing the input

```shell
bin/standardize_input.py --help
```

The purpose of this script is to
standardize and clean up
the training input we get from the Living Arabic project.
Adjust this script or create your own
to handle your data.

### Training the model

```shell
bin/train.py --help
```

This script uses the standardized training input
created by `standardize_input.py` to train the model.
`train.py` will output to `data/weights.csv` upon completion.
You can check the weights at any time in the log file,
`train.log` provided you're using log level `INFO` or `DEBUG`.

When training the model,
we found that a sample size of 2000 and
300 epochs was sufficient to
generate a good set of weights.
To use different values, pass them as input to `train.py`,
or change the defaults in `config.json`.

### Testing the model

```shell
bin/assess_terms_from_examples.py --help
```

## Applying the model to data

Once the dependencies are installed,
you can use the scripts provided
to process data.
To process new example data with the given model,
see the documentation for:

```shell
bin/terms_from_examples.py
```

You can execute the file directly from within the virtual environment
or run it with python directly:

```shell
bin/terms_from_examples.py --help

python bin/terms_from_examples.py --help
```

## Development

If you're making changes, be sure to run tests.

```shell
bin/test.sh --help
```

The test will run automatically as part of the CI pipelene when pushed to gitlab.
See `.gitlab-ci.yml` for details.

## Notes on using AraBERT and friends

[AraBERT][arabert_github] is based on Google's BERT
but uses Arabic inputs for training.
We use AraBERTv2 for our proof of concept.

Using AraBERT is just like using any other BERT-based model,
except that the text must be preprocessed to
isolate root words.

```python
from transformers import BertConfig, AutoTokenizer, AutoModel
from arabert.preprocess import ArabertPreprocessor

model_name = "aubmindlab/bert-base-arabertv2"

text = "ولن نبالغ إذا قلنا إن هاتف أو كمبيوتر المكتب في زمننا هذا ضروري"

preprocessor = ArabertPreprocessor(model_name=model_name)
tokenizer = AutoTokenizer.from_pretrained(model_name)
# We need to get the attentions for our analysis
config = BertConfig.from_pretrained(model_name, output_hidden_states=True, output_attentions=True)
model = AutoModel.from_pretrained(model_name, config=config)

# Separate roots
text_p = preprocessor.preprocess(text)
# و+ لن نبالغ إذا قل +نا إن هاتف أو كمبيوتر ال+ مكتب في زمن +نا هذا ضروري

# Get term ids
text_t = tokenizer.encode(text_p, return_tensors="pt")
# tensor([[   33,    29,  1023, 28880,   985,  1457,     8,   348,  3259,   347, 4989,    20,  1186,   289,  2407,     8,   387,  3368,    34]])

output = model(text_t)

print("This should be 13: input embedding (index 0) + 12 hidden layers (indices 1 to 12)")
print(len(output[2]))
print("\n")

print("For each of these 13: 1,19,768 = input sequence, index of each input id in sequence, size of hidden layer")
print(output[2][0].shape)
print("\n")

print("This should be the attention for each of the 12 layers")
print(len(output[3]))
# 12
print("\n")

print("0 index = first layer, 1,12,19,19 = , layer, index of each input id in sequence, index of each input id in sequence")
print(output[3][0].shape)
# torch.Size([1, 12, 19, 19])
print("\n")

print("Attentions easy access as output.attentions")
print(output.attentions == output[3])
# True
print("\n")

print("Attention nodes look like:")
print(output.attentions[0][0][11][5][3])
# tensor(0.0572, grad_fn=<SelectBackward>)
print("\n")
```

[arabert_github]: https://github.com/aub-mind/arabert
[clark2019]: https://nlp.stanford.edu/pubs/clark2019what.pdf
[living_arabic]: https://livingarabic.com/
