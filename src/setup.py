"""
examples_to_terms

Given examples, identify corresponding terms
"""
from setuptools import setup, find_packages
setup(
    name="examples_to_terms",
    packages=find_packages(),
    description="Given examples, identify corresponding terms",
)
