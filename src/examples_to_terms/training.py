"""
training.py

Functions around training the model
"""

import logging
import torch

from examples_to_terms.text_processing import arabic_string_to_ids

from examples_to_terms.criteria import arabic_id_idx

from examples_to_terms.model import model

logger = logging.getLogger(__name__)


def training_loop(
    arabic_examples,
    target_terms,
    n_epochs,
    weights,
    optimizer,
):
    """
    Train the model using the given specs

    :param list arabic_examples: A list of arabic example strings corresponding to target terms
    :param list target_terms: A list of arabic target terms corresponding to examples
    :param int n_epochs: A positive integer for the number of epochs (training cycles)
    :param list weights: a list of 12 floats representing starting weights
    :param torch.optim.Optimizer optimizer: A pytorch optimizer, like Adam
    :return: An optimized set of weights for BERT-style attention layers
    :rtype: torch.Tensor
    """
    for epoch in range(n_epochs):
        losses = torch.zeros(len(arabic_examples))
        for i, (example, target) in enumerate(zip(arabic_examples, target_terms)):
            if i % 1000 == 0:
                logger.info("Assessing epoch %s, example %s", epoch, i)
            term_foci = model(example, weights)
            target_t = _get_target_vector(example, target)
            losses[i] = loss_fn(term_foci, target_t)

        net_loss = losses.mean()

        optimizer.zero_grad()
        net_loss.backward(retain_graph=True)
        optimizer.step()
        logger.info("Completed epoch %s with loss %s and weights %s", epoch, net_loss, weights)

    return weights


def loss_fn(term_foci, target_t):
    """
    Get the loss for the application of the model to a single example

    If all foci are equal, consider it a win if there are at most 2 terms,
    a loss otherwise.

    :param torch.Tensor term_foci: A tensor for non-normalized term weights
    :param torch.Tensor target_t: A tensor representing
        the location of the target term in the example.
        Has 1.0 at the target and zeros elsewhere.
    :return: The loss for the given application of the model and target term
    :type: torch.Tensor
    """
    loss = 0.0
    min_focus, max_focus = torch.min(term_foci), torch.max(term_foci)
    normalized_foci = (
        (term_foci - min_focus)/(max_focus - min_focus) if max_focus != min_focus
        else torch.ones(len(term_foci)) if len(term_foci) <= 2
        else torch.zeros(len(term_foci))
    )
    loss = (1 - torch.sum(normalized_foci*target_t))**2
    return loss


def _get_target_vector(arabic_example, target_term):
    target_idx = arabic_id_idx(target_term, arabic_example)
    example_ids = arabic_string_to_ids(arabic_example)[0]
    target_t = torch.zeros(len(example_ids))
    if target_idx >= 0:
        target_t[target_idx] = 1.0
    else:
        logger.warning(" ".join([
            f"The example '{arabic_example}'",
            f"doesn't contain the target '{target_term}'"
        ]))
    return target_t
