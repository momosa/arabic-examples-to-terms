"""
text_processing.py

Prepare data for training
"""

import logging
from examples_to_terms.singletons import (
    get_arabic_preprocessor,
    get_arabic_tokenizer,
    get_arabic_model,
    get_english_tokenizer,
    get_english_model,
)

logger = logging.getLogger(__name__)


def arabic_string_to_tokens(string):
    """
    Break an Arabic string into tokens

    :param str string: An Arabic string
    :return: The string with roots, prefixes, etc. space-separated
    :rtype: str
    """
    preprocessor = get_arabic_preprocessor()
    string_p = preprocessor.preprocess(string)
    return string_p


def arabic_string_to_ids(string):
    """
    Get the IDs correspinding to the string's tokens

    The returned Tensor is two-deep,
    so the Tensor with the IDs is ids[0]

    :param str string: An Arabic string
    :return: An encoded tensor with ids for the string
    :rtype: torch.Tensor
    """
    tokenizer = get_arabic_tokenizer()
    string_p = arabic_string_to_tokens(string)
    ids = tokenizer.encode(string_p, return_tensors="pt")
    return ids


def arabic_ids_to_tokens(ids):
    """
    Get the tokens corresponding to the ids

    :param torch.Tensor ids: Contains ids at ids[0],
        which is the form returned by tokenizer.encode
        to get the ids in the first place
    :return: Strings corresponding to each ID
    :rtype: list
    """
    tokenizer = get_arabic_tokenizer()
    try:
        tokens = tokenizer.convert_ids_to_tokens(ids[0])
    except IndexError:
        logger.critical("Cannot turn %s into tokens.", ids)
    return tokens


def arabic_evaluate(string):
    """
    Get the term foci using the model

    :param str string: An arabic string
    :return: A non-normalized list of floats corresponding to term foci.
    :rtype: list
    """
    model = get_arabic_model()
    model.eval()
    ids = arabic_string_to_ids(string)
    result = model(ids)
    return result


def english_string_to_ids(string):
    """
    Get the IDs correspinding to the string's tokens

    The returned Tensor is two-deep,
    so the Tensor with the IDs is ids[0]

    :param str string: An English string
    :return: An encoded tensor with ids for the string
    :rtype: torch.Tensor
    """
    tokenizer = get_english_tokenizer()
    ids = tokenizer.encode(string, return_tensors="pt")
    return ids


def english_ids_to_tokens(ids):
    """
    Get the tokens corresponding to the ids

    :param torch.Tensor ids: Contains ids at ids[0],
        which is the form returned by tokenizer.encode
        to get the ids in the first place
    :return: Strings corresponding to each ID
    :rtype: list
    """
    tokenizer = get_english_tokenizer()
    tokens = tokenizer.convert_ids_to_tokens(ids[0])
    return tokens


def english_evaluate(string):
    """
    Get the term foci using the model

    :param str string: An English string
    :return: A non-normalized list of floats corresponding to term foci.
    :rtype: list
    """
    model = get_english_model()
    model.eval()
    ids = english_string_to_ids(string)
    result = model(ids)
    return result
