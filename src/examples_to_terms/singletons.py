"""
singletons.py

Minimize duplicating expensive things

We use a functional singleton pattern for two reasons.
First, the expensive objects are only created as needed.
Second, this functional pattern is more straightforward than class patterns.
The drawback is that we use mutable private globals to achieve this pattern.
These globals are clearly named as private to the module
and they are only used in the function immediately following.
To avoid unintentional misuse,
we've narrowly-scoped disabling lint rules.
"""


import logging
from transformers import AutoTokenizer, BertConfig, AutoModel

from arabert.preprocess import ArabertPreprocessor

from examples_to_terms.config import (
    ARABIC_MODEL_NAME,
    ENGLISH_MODEL_NAME,
)

logger = logging.getLogger(__name__)

_arabic_tokenizer = None #pylint: disable=invalid-name
def get_arabic_tokenizer():
    """
    A singleton tokenizer for the arabic model

    Usage:

        tokenizer = get_arabic_tokenizer()

    :return: A tokenizer for the arabic model
    :rtype: transformers.AutoTokenizer
    """
    global _arabic_tokenizer #pylint: disable=global-statement,invalid-name
    if _arabic_tokenizer is None:
        logger.info("Initializing tokenizer for %s", ARABIC_MODEL_NAME)
        _arabic_tokenizer = AutoTokenizer.from_pretrained(ARABIC_MODEL_NAME)
    return _arabic_tokenizer


_english_tokenizer = None #pylint: disable=invalid-name
def get_english_tokenizer():
    """
    A singleton tokenizer for the english model

    Usage:

        tokenizer = get_english_tokenizer()

    :return: A tokenizer for the english model
    :rtype: transformers.AutoTokenizer
    """
    global _english_tokenizer #pylint: disable=global-statement,invalid-name
    if _english_tokenizer is None:
        logger.info("Initializing tokenizer for %s", ENGLISH_MODEL_NAME)
        _english_tokenizer = AutoTokenizer.from_pretrained(ENGLISH_MODEL_NAME)
    return _english_tokenizer


_arabic_preprocessor = None #pylint: disable=invalid-name
def get_arabic_preprocessor():
    """
    A singleton preprocessor for the arabic model

    Usage:

        preprocessor = get_arabic_preprocessor()

    :return: A preprocessor for the Arabic model
    :rtype: arabert.preprocess.ArabertPreprocessor
    """
    global _arabic_preprocessor #pylint: disable=global-statement,invalid-name
    if not _arabic_preprocessor:
        logger.info("Initializing preprocessor for %s", ARABIC_MODEL_NAME)
        _arabic_preprocessor = ArabertPreprocessor(model_name=ARABIC_MODEL_NAME)
    return _arabic_preprocessor


_arabic_model = None #pylint: disable=invalid-name
def get_arabic_model():
    """
    A singleton for the arabic model

    Usage:

        model = get_arabic_model()

    :return: The arabic model
    :rtype: transformers.AutoModel
    """
    global _arabic_model #pylint: disable=global-statement,invalid-name
    if not _arabic_model:
        config = BertConfig.from_pretrained(
            ARABIC_MODEL_NAME,
            output_hidden_states=True,
            output_attentions=True,
        )
        logger.info("Initializing model for %s", ARABIC_MODEL_NAME)
        _arabic_model = AutoModel.from_pretrained(ARABIC_MODEL_NAME, config=config)
    return _arabic_model


_english_model = None #pylint: disable=invalid-name
def get_english_model():
    """
    A singleton for the english model

    Usage:

        model = get_english_model()

    :return: The english model
    :rtype: transformers.AutoModel
    """
    global _english_model #pylint: disable=global-statement,invalid-name
    if not _english_model:
        config = BertConfig.from_pretrained(
            ENGLISH_MODEL_NAME,
            output_hidden_states=True,
            output_attentions=True,
        )
        logger.info("Initializing model for %s", ENGLISH_MODEL_NAME)
        _english_model = AutoModel.from_pretrained(ENGLISH_MODEL_NAME, config=config)
    return _english_model
