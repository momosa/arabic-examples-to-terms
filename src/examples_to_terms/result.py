"""
result.py
"""

import string
import logging

from examples_to_terms import arabic
from examples_to_terms.config import CSV_DELIMITER

logger = logging.getLogger(__name__)


class Result:
    """
    Class representing the result.

    In addition to applying the model,
    ArabicTermResult streamlines removing delimiters, normalization, and formatting output.

    :param ArabicTermResult arabic_term_result:
    """

    dictionary = []


    @classmethod
    def set_dictionary(cls, dictionary):
        """
        Set the dictionary for the whole class.
        :classmethod:

        :param list dictionary: a list of DictionaryTerm
        :param string english_example:
        """
        cls.dictionary = dictionary
        logger.debug("Dictionary set to %s ...", cls.dictionary[:3])


    def __init__(self, arabic_term_result, english_example):
        if not self.dictionary:
            raise ValueError(
                "Please set the dictionary using the set_dictionary classmethod"
            )

        self.terms = self._get_terms_in_dictionary(arabic_term_result)
        self._target_position = None


        # The following only used for output
        self.preprocessed_arabic = arabic_term_result.preprocessed_arabic
        self.arabic_example = arabic_term_result.arabic_example
        self.target_term = arabic_term_result.target_term
        self.target_definition = None
        if self.target_term:
            self.target_definition = next((
                t.definition for t in self.dictionary
                if arabic.equals(t.word, self.target_term)
                and arabic.equals(t.arabic_example, self.arabic_example)
            ), None)
        self.english_example = english_example
        logger.debug("arabic_example set to %s", self.arabic_example)
        logger.debug("target_term set to %s", self.target_term)
        logger.debug("english_example set to %s", self.english_example)

        for term in self.terms:
            term["english_meaning_weight"] = _score_english_meaning(
                term["definition"],
                english_example,
            )

        self._aggregate_weights()
        self._normalize_aggregates()

        self.terms.sort(key=lambda t: t["aggregate_weight"], reverse=True)


    @property
    def target_position(self):
        """
        The index of the target word/definition in the available candidates
        :property:

        Ideally, this is 0.

        :return: The index of the target word/definition in the list of candidates,
            -1 if the term isn't one of the candidates,
            None if there is no target word
        :rtype: int
        """
        if self._target_position is None and self.target_term:
            self._target_position = -1
            for idx, term in enumerate(self.terms):
                if all([
                    arabic.equals(term["token"], self.target_term),
                    arabic.equals(term["definition"], self.target_definition),
                ]):
                    self._target_position = idx
                    break
        return self._target_position


    def as_string(self):
        """
        Get a formatted string representing the result.

        :return: An easy-to-read result
        :rtype: str
        """
        lines = [
            f"Pre-processed Arabic: {self.preprocessed_arabic}",
            f"Processed Arabic: {self.arabic_example}",
            f"English: {self.english_example}",
            f"Target term: {self.target_term}",
            f"Target definition: {self.target_definition}",
            f"{'Agg Weight':>16s}{'Term Weight':>16s}{'Meaning Weight':>16s}{'Token':>10s}",
            "\n".join([
                ("".join([
                    f"{t['aggregate_weight']:>16f}",
                    f"{t['arabic_term_weight']:>16f}",
                    f"{t['english_meaning_weight']:>16f}",
                    f"{t['token']:>10s}",
                ])) for t in self.terms
            ]),
            f"{'Token':>16s}Definition",
            "\n".join([
                f"{t['token']:>16s}{t['definition']}"
                for t in self.terms
            ])
        ]
        return "\n".join(lines)


    @staticmethod
    def csv_header(delimiter=CSV_DELIMITER):
        """
        Get the header for the csv file.

        We don't know how many terms there are,
        so that part just repeats.

        :param str delimiter: The csv delimiter
        :return: The header line for a csv
        :rtype: str
        """
        csv_items = [
            "Pre-processed Arabic example",
            "Arabic example",
            "English example",
            "Target term",
            "Target definition",
            "Token",
            "Token aggregate weight",
            "Token term weight",
            "Token English meaning weight",
        ]
        return delimiter.join(csv_items)


    def as_csv_string(self, delimiter=CSV_DELIMITER):
        """
        Get a csv string as a result.

        :param str delimiter: The csv delimiter
        :return: A row for a csv file
        :rtype: str
        """
        term_csv = lambda term: delimiter.join([
            term["token"],
            str(term["aggregate_weight"]),
            str(term["arabic_term_weight"]),
            str(term["english_meaning_weight"]),
        ])
        all_terms_csv = delimiter.join(
            [term_csv(t) for t in self.terms]
        )
        try:
            csv_items = [
                self.preprocessed_arabic or "",
                self.arabic_example,
                self.english_example,
                self.target_term or "",
                self.target_definition or "",
                all_terms_csv,
            ]
        except TypeError:
            print(self)
            raise
        return delimiter.join(csv_items)

    def __iter__(self):
        """
        Make the result iterable.

        Allows for dict(result)
        """
        keys = [
            "preprocessed_arabic",
            "arabic_example",
            "english_example",
            "target_term",
            "target_definition",
            "terms",
        ]
        for key in keys:
            yield(key, getattr(self, key))


    def _get_terms_in_dictionary(self, arabic_term_result):
        terms = []
        term_hashes = set()

        get_dictionary_terms = lambda word: [
            t for t in self.dictionary
            if arabic.equals(t.word, word)
        ]

        for candidate in arabic_term_result.terms:
            dictionary_terms = get_dictionary_terms(candidate["token"])
            for dict_term in dictionary_terms:
                dict_term_hash = hash(dict_term.word + dict_term.definition)
                if dict_term_hash not in term_hashes:
                    terms.append({
                        "token": dict_term.word,
                        "definition": dict_term.definition,
                        "arabic_term_weight": candidate["weight"],
                        "english_meaning_weight": None,
                        "aggregate_weight": None
                    })
                    term_hashes.add(dict_term_hash)

        logger.debug(
            "All terms contained in dictionary"
            if len(terms) == len(arabic_term_result.terms)
            else (
                "Terms not found in dictionary: %s",
                [
                    t["token"] for t in arabic_term_result.terms
                    if len(get_dictionary_terms(t["token"])) == 0
                ]
            )
        )

        return terms

    def _aggregate_weights(self):
        for term in self.terms:
            term["aggregate_weight"] = (
                0.5*term["arabic_term_weight"] +
                0.5*term["english_meaning_weight"]
            )


    def _normalize_aggregates(self):
        total = sum(t["aggregate_weight"] for t in self.terms)
        for term in self.terms:
            term["aggregate_weight"] /= total


    def __repr__(self):
        return ("<" + ", ".join([
            f"preprossed_arabic: {self.preprocessed_arabic}",
            f"arabic_example={self.arabic_example}",
            f"english_example={self.english_example}",
            f"target_term={self.target_term}",
            f"target_definition={self.target_definition}",
            f"terms={self.terms}",
        ]) + ">")


def _score_english_meaning(meaning, example):
    standardize = lambda t: t.translate(str.maketrans("", "", string.punctuation)).lower()
    meaning_tokens = standardize(meaning).split(" ")
    example_tokens = standardize(example).split(" ")
    top_len = max(len(t) for t in example_tokens)
    min_score = 0.25
    score = min_score
    get_term_score = lambda t: 1.0*len(t)*(1.0 - min_score)/top_len + min_score
    for meaning_token in meaning_tokens:
        if len(meaning_token) > 2 and meaning_token in example_tokens:
            score = max(score, get_term_score(meaning_token))
    return score
