"""
config

Config options are given in [project_dir]/config.json

Usage:

    from examples_to_terms.config import DEFAULT_LOG_LEVEL
    print(DEFAULT_LOG_LEVEL)
    # INFO
"""

from pathlib import Path
import json

CONFIG_FILE = Path(__file__).parents[3] / "config.json"


def _from_file():
    configs = None
    with open(CONFIG_FILE, "r", encoding="utf-8") as config_file:
        configs = json.load(config_file)
    return configs

def _to_text_list(configs):
    assignments = [
        f"{key} = '{value}'" if isinstance(value, str) else f"{key} = {value}"
        for key, value in configs.items()
    ]
    return assignments

CONFIG_DATA = _from_file()
ASSIGNMENT_LIST = _to_text_list(CONFIG_DATA)
exec("\n".join(ASSIGNMENT_LIST)) #pylint: disable=exec-used
