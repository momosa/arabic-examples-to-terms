"""
dictionary_term.py
"""

import logging

from examples_to_terms.criteria import (
    is_word_recognized,
    is_word_in_example,
)

logger = logging.getLogger(__name__)


class DictionaryTerm:
    """
    Class for clarifying dictionary terms.

    Parses data from the user defined dictionary

    :param str word:
    :param str definition:
    :param str arabic_example:
    :param str english_example:
    """

    idx = None


    def __init__(self, word, definition, arabic_example, english_example):
        self.word = word
        logger.debug("word set to %s", self.word)

        self.definition = definition
        logger.debug("definition set to %s", self.definition)

        self.arabic_example = arabic_example
        logger.debug("arabic_example set to %s", self.arabic_example)
        self.english_example = english_example
        logger.debug("english_example set to %s", self.english_example)

        if not self.arabic_example:
            raise ValueError(f"Illegitimate Arabic example '{arabic_example}'")
        if not self.english_example:
            raise ValueError(f"Illegitimate English example '{english_example}'")


    def is_target_recognized(self):
        """
        Determine if the tokenizer can handle the target word
        """
        return is_word_recognized(self.word)


    def is_word_in_example(self):
        """
        Determine if the target word is recognized in the Arabic example
        """
        return is_word_in_example(self.word, self.arabic_example)


    def __repr__(self):
        return (
            "<" + ", ".join([
                f"word={self.word}",
                f"definition={self.definition}",
                f"arabic_example={self.arabic_example}",
                f"english_example={self.english_example}",
            ]) + ">"
        )
