"""
model.py
"""

import logging
import torch

from examples_to_terms.text_processing import arabic_evaluate
from examples_to_terms.config import CACHE_SIZE

logger = logging.getLogger(__name__)


ATTENTIONS_CACHE = {}


def model(arabic_example, weights):
    """
    The following are equivalent:

        attention_grid = torch.einsum("i,ijk->jk", weights, attentions)
        attention_grid = torch.sum(weights * torch.transpose(attentions, 0, 2), 2)

    I used einsum because I find it more clear and I suspect it's faster.

    :param str arabic_example:
    :param torch.tensor weights: Twelve weights
    :return: Relative weights attached to each term, non-normalized
    :rtype: torch.tensor
    """
    attentions = _get_attentions(arabic_example)
    attention_grid = torch.einsum("i,ijk->jk", weights, attentions)
    term_foci = torch.sum(attention_grid, 0) #pylint: disable=no-member
    term_foci = term_foci/torch.sum(term_foci) #pylint: disable=no-member
    return term_foci


def _get_attentions(arabic_example):
    attentions = None
    if arabic_example in ATTENTIONS_CACHE:
        attentions = ATTENTIONS_CACHE[arabic_example]
    else:
        attentions = arabic_evaluate(arabic_example).attentions[0][0]
        if len(ATTENTIONS_CACHE) < CACHE_SIZE:
            ATTENTIONS_CACHE[arabic_example] = attentions
    return attentions
