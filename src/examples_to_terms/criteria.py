"""
criteria.py

Streamlined criteria used to selecting rows for
training or applying the model.
"""

import logging
from pyarabic import araby

from examples_to_terms.text_processing import arabic_string_to_tokens

from examples_to_terms import arabic


logger = logging.getLogger(__name__)


def is_dialect(actual, expected):
    """
    :param str actual:
    :param str expected:
    :return: whether or not the two input dialects are the same
    :rtype: bool
    """
    return actual.casefold() == expected.casefold()


def arabic_id_idx(word, string, strip_tashkeel=True):
    """
    Get the index of the word id in the string.

    :param str word: An arabic word, may be a root or base
    :param str string: An arabic string, like a word or sentence
    :return: The index of the word in the string if the word contains the string, -1 otherwise.
        The position of the index is relative to the tokens.
    :rtype: int
    """
    id_idx = -1
    complete = False
    tokens=[]

    if len(word) == 0 or len(string) == 0:
        complete = True

    if not complete:
        if strip_tashkeel:
            word = araby.strip_tashkeel(word)
            string = araby.strip_tashkeel(string)
        else:
            word = araby.strip_harakat(word)
            string = araby.strip_harakat(string)
        tokens = arabic_string_to_tokens(string).split(" ")

    if len(word) > 1 and arabic.equals(word[-1], "ﺓ"):
        word = word[:-1]

    for token_idx, token in enumerate(tokens):
        if arabic.startswith(token, word):
            id_idx = token_idx + 1
            complete = True
            break

    if not complete and len(word) > 2 and (araby.is_alef(word[0]) or araby.is_hamza(word[0])):
        truncated_word = word[1:]
        candidates = (
            i+1 for i, t in enumerate(tokens)
            if arabic.contains(t, truncated_word)
        )
        id_idx = next(candidates, -1)
        complete = id_idx != -1

    if not complete and len(arabic.remove_unstable(word)) > 1:
        stable_word = arabic.remove_unstable(word)
        candidates = (
            i+1 for i, t in enumerate(tokens)
            if arabic.contains(arabic.remove_unstable(t), stable_word)
        )
        id_idx = next(candidates, -1)
        complete = id_idx != -1

    logger.debug(
        "Word '%s' has idx %s in string '%s' with tokens %s.",
        word,
        id_idx,
        string,
        tokens,
    )

    return id_idx


def is_word_recognized(string):
    """
    Determine if the word is recognized by AraBERT

    :param str string: A single word
    :return: whether or not AraBERT recognizes the string
    :rtype: bool
    """
    recognized = arabic_id_idx(string, string) != -1
    logger.debug(
        "%s term '%s'",
        "Recognized" if recognized else "Failed to recognize",
        string,
    )
    return recognized


def is_word_in_example(word, example):
    """
    Determine if the example contains the word

    The word is typically a root.
    The example tokens we derive are typically base words.

    :param str word: A single word
    :param str example: One or more words
    :return: whether or not the word is in the example
    :rtype: bool
    """
    word_in_example = arabic_id_idx(word, example) != -1
    logger.debug(
        "%s term %s in example %s with tokens %s",
        "Found" if word_in_example else "Failed to find",
        word,
        example,
        arabic_string_to_tokens(example).split(" "),
    )
    return word_in_example
