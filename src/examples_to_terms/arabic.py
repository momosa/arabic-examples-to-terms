"""
arabic.py

Functions for handling arabic strings
"""

import unicodedata


UNSTABLE = "و ي ا ء ؤ ئ أ إ ى"

def _normalize(
        word: str,
    ) -> str:
    """
    Normalize the word for comparison.
    Handles arabic initial/medial/final char forms.
    May also need to casefold lhs.

    :param str word: An arabic word
    :return: a normalized string
    :rtyle: str
    """
    return unicodedata.normalize('NFKD', word)


def equals(
        word1: str,
        word2: str,
    ) -> bool:
    """
    Determine whether or not two strings are equal

    :param str word1: An arabic word
    :param str word2: An arabic word
    :return: Whether or not the strings are equal
    :rtype: bool
    """
    return _normalize(word1).casefold() == _normalize(word2)


def startswith(
        string: str,
        candidate_initial_substring: str,
    ) -> bool:
    """
    Determine whether a string starts with another

    :param str string: An arabic word
    :param str candidate_initial_substring: An arabic word
    :return: Whether or not the first string starts with the second
    :rtype: bool
    """
    return _normalize(string).casefold().startswith(_normalize(candidate_initial_substring))


def contains(
        string: str,
        candidate_substring: str,
    ) -> bool:
    """
    Determine whether or not a string contains another

    :param str string: An arabic word
    :param str candidate_substring: An arabic word
    :return: Whether or not the second string contains the first
    :rtype: bool
    """
    return _normalize(candidate_substring).casefold() in _normalize(string)


def remove_unstable(
        word: str,
    ) -> str:
    """
    Remove unstanble characters from the word

    :param str word: An Arabic word
    :return: The word, with unstable characters removed
    :rtype: str
    """
    is_unstable = lambda c: any(equals(c, u) for u in UNSTABLE.split(" "))
    stable = "".join(char for char in word if not is_unstable(char))
    return stable
