"""
indata.py

Get input data from files
"""

import sys
import logging
import csv
import re

from pyarabic import araby

from examples_to_terms.config import (
    DIALECTS,
    CSV_DELIMITER,
    INPUT_COLUMNS,
)

logger = logging.getLogger(__name__)


def get_input_data(input_dir, dryrun=False):
    """
    Get the input data from all files in the given directory

    :param pathlib.Path input_dir: Absolute path to the directory containing input files
    :param bool dryrun: Used to limit processing
    :return: Standardized input data
    :rtype: list
    """
    data_arr = []
    input_files = [f for f in input_dir.iterdir() if f.is_file()]
    logger.debug("input_files: %s", input_files)
    for input_file in input_files:
        logger.info("Getting data from %s", input_file)
        data_arr += _get_data_from_file(input_file, dryrun)
        logger.info("Finished getting data from %s", input_file)
    return data_arr


def get_examples(example_entry):
    """
    Separate examples out from the entry.

    Sample entry:

        يا بَشَر شُفْتُوا القُمَر (الگُمَر) [I] People, have you seen the moon?

    :param str example_entry: String containing the Arabic and English examples
    :return: English and Arabic examples
    :rtype: tuple
    """
    arabic = ""
    english = ""
    if isinstance(example_entry, str):
        parts = re.split(r"\[[A-Za-z-_]+\]", example_entry)
        if len(parts) == 2:
            arabic, english = parts
            logger.debug("""
                Parsed example into
                arabic: '%s', english: '%s'
            """, arabic, english)
        else:
            logger.warning("Unable to parse example_entry '%s'", example_entry)
        #arabic = araby.strip_harakat(arabic)
        arabic = araby.strip_tashkeel(arabic)
        arabic = arabic.strip().lower()
        english = english.strip().lower()
    return (english, arabic)


def _get_data_from_file(input_file, dryrun=False):
    data_arr = []
    maxrows = 10 if dryrun else sys.maxsize

    try:
        with open(input_file, mode="r", encoding="utf-8") as messy_input_file:
            messy_data = list(csv.reader(messy_input_file, delimiter=CSV_DELIMITER))
    except UnicodeDecodeError:
        logger.critical(
            "Dictionary file '%s' is not in utf-8 format",
            input_file,
        )
        raise

    dialect = ""
    word = ""
    meaning = ""
    for row in messy_data:
        if len(data_arr) >= maxrows:
            break
        dialect = _standardize_dialect(row[INPUT_COLUMNS["dialect"]]) or dialect
        word = _standardize_word(row[INPUT_COLUMNS["word"]]) or word
        meaning = _standardize_meaning(row[INPUT_COLUMNS["meaning"]]) or meaning
        for example in row[INPUT_COLUMNS["examples"]].split("\n"):
            if len(data_arr) >= maxrows:
                break
            arabic_example, english_example = get_examples(example)
            # !!! Arabic and english example order will be switched because... Arabic?
            standardized_row = [dialect, word, meaning, english_example, arabic_example]
            if _validate_row_data(*standardized_row):
                logger.debug("Adding data: %s", standardized_row)
                data_arr.append(standardized_row)
            else:
                logger.debug("Unable to validate row %s", standardized_row)

    return data_arr


def _validate_row_data(dialect, word, meaning, english_example, arabic_example):
    return dialect in DIALECTS and all((
        isinstance(item, str) and len(item) > 0 and len(item) < 512
        for item in [word, meaning, arabic_example, english_example]
    ))


def _standardize_dialect(dialect):
    standardized = ""
    if isinstance(dialect, str):
        standardized = dialect.split("_")[0].strip()
    logger.debug("Standardized dialect '%s' to '%s'", dialect, standardized)
    return standardized


def _standardize_word(word):
    standardized = ""
    if isinstance(word, str):
        standardized = re.sub(r"\[[A-Za-z]+\]", "", word)
        standardized = re.sub(r"\n", " ", standardized)
        standardized = re.sub(r"[\s]+", " ", standardized)
        #standardized = araby.strip_harakat(standardized)
        standardized = araby.strip_tashkeel(standardized)
        standardized = standardized.split("/")[0]
        standardized = standardized.split("،")[0]
        standardized = standardized.strip().lower()
    logger.debug("Standardized word '%s' to '%s'", word, standardized)
    return standardized


def _standardize_meaning(meaning):
    standardized = ""
    if isinstance(meaning, str):
        standardized = meaning.split(":")[1] if ":" in meaning else meaning
        standardized = re.sub(r"\n", " ", standardized)
        standardized = re.sub(r"[\s]+", " ", standardized)
        standardized = standardized.strip().lower()
    logger.debug("Standardized meaning '%s' to '%s'", meaning, standardized)
    return standardized
