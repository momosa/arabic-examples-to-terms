"""
arabic_term_result.py
"""

import logging
import torch

from examples_to_terms.model import model

from examples_to_terms.text_processing import (
    arabic_string_to_ids,
    arabic_ids_to_tokens,
)

logger = logging.getLogger(__name__)


class ArabicTermResult:
    """
    Class representing the ranked set of Arabic terms for the Arabic example.

    In addition to applying the model,
    ArabicTermResult streamlines removing delimiters, normalization, and formatting output.

    :param str arabic_example:
    :param str target_term:
    """

    weights = None


    @classmethod
    def set_weights(cls, weights):
        """
        Set the weights for the whole class.
        :classmethod:

        :param list weights: A list of exactly 12 floats
        """
        if len(weights) != 12:
            raise ValueError(f"{weights} does not have exactly 12 weights.")
        cls.weights = weights
        logger.debug("Weights set to %s", cls.weights)


    def __init__(self, arabic_example, preprocessed_arabic=None, target_term=None):
        if ArabicTermResult.weights is None:
            raise ValueError(" ".join([
                "Please set the weights using the classmethod",
                "ArabicTermResult.set_weights(list_of_twelve_weights)",
            ]))
        self.arabic_example = arabic_example
        self.preprocessed_arabic = preprocessed_arabic
        self.target_term = target_term
        self.target_id = None
        if target_term:
            self.target_id = int(arabic_string_to_ids(self.target_term)[0][1])
        logger.debug("arabic_example set to %s", self.arabic_example)
        logger.debug("target_term set to %s", self.target_term)
        logger.debug("target_id set to %s", self.target_id)

        term_foci = model(arabic_example, torch.tensor(ArabicTermResult.weights))
        ids = arabic_string_to_ids(arabic_example)
        self.terms = [
            {
                "id": int(ids[0][i]),
                "token": arabic_ids_to_tokens(_as_encoded(ids[0][i]))[0],
                "weight": float(term_foci[i]),
            } for i in range(len(ids[0]))
        ]
        logger.debug("Pre-adjusted terms are %s", self.terms)
        self._target_position = None
        self._remove_delimiters()
        self._remove_prefixes()
        self._remove_punctuation()
        self._normalize_weights()
        logger.debug("terms set to %s", self.terms)

        self.terms.sort(key=lambda t: t["weight"], reverse=True)


    @property
    def target_position(self):
        """
        The index of the target word in the available candidates.
        :property:

        Ideally, this is 0.

        :return: The index of the target word in the list of candidates,
            -1 if the term isn't one of the candidates.
        :rtype: int
        """
        if self._target_position is None and self.target_id is not None:
            self._target_position = -1
            for idx, term in enumerate(self.terms):
                if term["id"] == self.target_id:
                    self._target_position = idx
                    break
        return self._target_position


    def as_string(self):
        """
        Get a formatted string representing the result.

        :return: An easy-to-read result
        :rtype: str
        """
        lines = [
            f"Pre-processed Arabic: {self.preprocessed_arabic}",
            f"Processed: {self.arabic_example}",
            f"Target term: {self.target_term}",
            f"{'ID':>10s}{'Token':>10s}{'Weight':>10s}",
            "\n".join([
                f"{t['id']:>10d}{t['token']:>10s}{t['weight']:>10f}"
                for t in self.terms
            ]),
        ]
        return "\n".join(lines)


    def _remove_delimiters(self):
        delimiters = ("[CLS]", "[SEP]")
        self.terms = [
            t for t in self.terms
            if t["token"] not in delimiters
        ]


    def _remove_prefixes(self):
        self.terms = [
            t for t in self.terms
            if "+" not in t["token"]
        ]


    def _remove_punctuation(self):
        punctuation = "!.?()-;,:"
        self.terms = [
            t for t in self.terms
            if t["token"] not in punctuation
        ]


    def _normalize_weights(self):
        """
        Only call this after removing undesired items.
        """
        if len(self.terms) > 1:
            bottom = min([t["weight"] for t in self.terms])
            total = sum([t["weight"] - bottom for t in self.terms])
            if total == 0.0:
                total = 1.0
            for term in self.terms:
                term["weight"] = (term["weight"] - bottom)/total
        elif len(self.terms) == 1:
            self.terms[0]["weight"] = 1.0


    def __repr__(self):
        return ("<" + ", ".join([
            f"target_term={self.target_term}",
            f"arabic_example={self.arabic_example}",
            f"target_id={self.target_id}",
            f"terms={self.terms}",
        ]) + ">")


def _as_encoded(identifier):
    ids = torch.zeros(1, 1)
    ids[0][0] = identifier
    return ids
