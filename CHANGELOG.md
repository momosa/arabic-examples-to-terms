# Changelog

* **1.1** 
  - Added preprocessed Arabic examples to output
  - Output data in common formats: txt, csv, json
  - Clarify error messaging around bad dictionary formatting
  - Fix: Differentiate between evaluating examples and assessing the system

## 1.0: MVP

* **0.3** Publication
  - Improve support for arbitrary data sets
  - Improve documentation

* **0.2** Integrate English analysis
  - Integrate English meaning analysis
  - Increase Arabic token recognition
  - CI to ensure code integrity on merge
  - Linting to avoid bugs and increase readability
  - Integration testing to ensure basic script functionality
  - Unit testing for explicit analysis and avoiding bugs
  - Fix: Some Arabic comparisons inaccurate
  - Fix: Assessing only first words in Arabic examples

* **0.1** Attention-based AI
  - Read data from the Living Arabic Project
  - Train an AI to use attentions from AraBERT to
    select terms best representing given examples
  - Create a document scripts to streamline the interface
  - Enable a simple config file to streamline the interface
