"""
examples_to_terms

Given examples, identify corresponding terms
"""
from setuptools import setup, find_packages
setup(
    name="arabert",
    packages=find_packages(),
    description="AraBERT",
)
