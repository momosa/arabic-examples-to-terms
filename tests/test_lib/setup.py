"""
tests

Tests for the examples to terms package and server.
"""
from setuptools import setup, find_packages
setup(
    name='tests',
    packages=find_packages(),
    description="Tests for the examples to terms package and server",
)
