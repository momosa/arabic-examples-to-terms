"""
Commonly used mocks
Their functionality is consistent from test to test.

Define tailored output on a per-test basis.
"""


from test_examples_to_terms.mocks.mock_logging import MockLogger
