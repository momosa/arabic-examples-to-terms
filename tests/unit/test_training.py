"""
test_training.py

Tests for training and loss functions.
"""


from textwrap import dedent
import torch

from test_examples_to_terms.util import TestUnit

from examples_to_terms.training import loss_fn


class TestLossFn(TestUnit):
    """
    Test getting the loss for the model

    Not mocking torch to ensure we're using it properly
    """


    def test_complete_miss(self):
        """
        Ensure loss is 1 when term foci miss the target
        """
        errors = []
        for test_case in [
            {
                "term_foci": torch.tensor([0.0, 1.0]),
                "target_t": torch.tensor([1.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([0.5, 1.0]),
                "target_t": torch.tensor([1.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([-1.0, 0.0]),
                "target_t": torch.tensor([1.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([1.0, 0.0]),
                "target_t": torch.tensor([0.0, 1.0]),
            },
            {
                "term_foci": torch.tensor([1.0, 0.0, 0.2]),
                "target_t": torch.tensor([0.0, 1.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([0.0, 0.5, -0.2]),
                "target_t": torch.tensor([0.0, 0.0, 1.0]),
            },
            {
                "term_foci": torch.tensor([0.0, 0.1, 0.0]),
                "target_t": torch.tensor([0.0, 0.0, 1.0]),
            },
            {
                "term_foci": torch.tensor([0.0, 0.0, 0.0]),
                "target_t": torch.tensor([0.0, 0.0, 1.0]),
            },
            {
                "term_foci": torch.tensor([2.0, 2.0, 2.0]),
                "target_t": torch.tensor([0.0, 0.0, 1.0]),
            },
        ]:
            actual = loss_fn(**test_case)
            expected = 1.0
            if actual != expected:
                errors.append(dedent(f"""
                    loss_fn failed when target has the lowest focus.
                    term_foci: {test_case["term_foci"]}
                    target_t: {test_case["target_t"]}
                    Expected {expected} but got {actual}
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))


    def test_top_position(self):
        """
        Ensure loss is 0 when the target has the highest focus
        """
        errors = []
        for test_case in [
            {
                "term_foci": torch.tensor([0.0, 1.0]),
                "target_t": torch.tensor([0.0, 1.0]),
            },
            {
                "term_foci": torch.tensor([0.5, 0.0]),
                "target_t": torch.tensor([1.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([-1.0, 0.0]),
                "target_t": torch.tensor([0.0, 1.0]),
            },
            {
                "term_foci": torch.tensor([1.0, 0.0]),
                "target_t": torch.tensor([1.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([-1.3, -4.5]),
                "target_t": torch.tensor([1.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([1.0, 0.0, 0.2]),
                "target_t": torch.tensor([1.0, 0.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([0.0, 0.5, -0.2]),
                "target_t": torch.tensor([0.0, 1.0, 0.0]),
            },
            {
                "term_foci": torch.tensor([0.2, 0.0, 0.2]),
                "target_t": torch.tensor([0.0, 0.0, 1.0]),
            },
            {
                "term_foci": torch.tensor([-2.0, -2.0]),
                "target_t": torch.tensor([0.0, 1.0]),
            },
            {
                "term_foci": torch.tensor([3.0]),
                "target_t": torch.tensor([1.0]),
            },
        ]:
            actual = loss_fn(**test_case)
            expected = 0.0
            if actual != expected:
                errors.append(dedent(f"""
                    loss_fn failed when target has the highest focus.
                    term_foci: {test_case["term_foci"]}
                    target_t: {test_case["target_t"]}
                    Expected {expected} but got {actual}
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))


    def test_strict_monotic_increase(self):
        """
        Ensure loss strictly monotonically increases as relative focus decreases
        """
        errors = []
        for test_case in [
            {
                "better_input": {
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                    "term_foci": torch.tensor([0.0, 1.0, 2.0]),
                },
                "worse_input": {
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                    "term_foci": torch.tensor([0.0, 0.5, 2.0]),
                },
            },
            {
                "better_input": {
                    "target_t": torch.tensor([1.0, 0.0, 0.0]),
                    "term_foci": torch.tensor([-1.5, -1.0, -2.0]),
                },
                "worse_input": {
                    "target_t": torch.tensor([1.0, 0.0, 0.0]),
                    "term_foci": torch.tensor([-1.7, -1.0, -2.0]),
                },
            },
            {
                "better_input": {
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                    "term_foci": torch.tensor([3.0, 2.0, 1.0]),
                },
                "worse_input": {
                    "target_t": torch.tensor([1.0, 0.0, 0.0]),
                    "term_foci": torch.tensor([1.5, 3.0, 1.0]),
                },
            },
        ]:
            loss_better = loss_fn(**test_case["better_input"])
            loss_worse = loss_fn(**test_case["worse_input"])
            if loss_better >= loss_worse:
                errors.append(dedent(f"""
                    loss_fn didn't increase strictly monotonically as relative focus decreases
                    Expected loss for input to be less than loss for worse_input 
                    better:
                        target_t: {test_case["better_input"]["target_t"]}
                        term_foci: {test_case["better_input"]["term_foci"]}
                        loss: {loss_better}
                    worse:
                        target_t: {test_case["worse_input"]["target_t"]}
                        term_foci: {test_case["worse_input"]["term_foci"]}
                        loss: {loss_worse}
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))


    def test_same_relative_focus(self):
        """
        Ensure loss is the same when the relative focus is the same
        """
        errors = []
        for test_case in [
            {
                "lhs_input": {
                    "target_t": torch.tensor([1.0, 0.0]),
                    "term_foci": torch.tensor([1.0, 0.0]),
                },
                "rhs_input": {
                    "target_t": torch.tensor([1.0, 0.0]),
                    "term_foci": torch.tensor([2.0, 0.0]),
                },
            },
            {
                "lhs_input": {
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                    "term_foci": torch.tensor([0.0, 1.0, 2.0]),
                },
                "rhs_input": {
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                    "term_foci": torch.tensor([0.0, 0.5, 1.0]),
                },
            },
            {
                "lhs_input": {
                    "target_t": torch.tensor([1.0, 0.0, 0.0]),
                    "term_foci": torch.tensor([-1.0, -1.4, -2.0]),
                },
                "rhs_input": {
                    "target_t": torch.tensor([1.0, 0.0, 0.0]),
                    "term_foci": torch.tensor([-1.0, -1.4, -3.0]),
                },
            },
            {
                "lhs_input": {
                    "target_t": torch.tensor([1.0, 0.0, 0.0]),
                    "term_foci": torch.tensor([1.0, 2.0, 3.0]),
                },
                "rhs_input": {
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                    "term_foci": torch.tensor([2.0, 1.0, 3.0]),
                },
            },
        ]:
            loss_lhs = loss_fn(**test_case["lhs_input"])
            loss_rhs = loss_fn(**test_case["rhs_input"])
            if loss_lhs != loss_rhs:
                errors.append(dedent(f"""
                    loss_fn failed to yield the same result for the same relative focus
                    Expected lhs and rhs to yield the same loss
                    lhs:
                        target_t: {test_case["lhs_input"]["target_t"]}
                        term_foci: {test_case["lhs_input"]["term_foci"]}
                        loss: {loss_lhs}
                    rhs:
                        target_t: {test_case["rhs_input"]["target_t"]}
                        term_foci: {test_case["rhs_input"]["term_foci"]}
                        loss: {loss_rhs}
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))


    def test_penalty_increase(self):
        """
        Ensure loss increase wrt focus decrease is more than linear
        """
        errors = []
        for test_case in [
            {
                "target_t": torch.tensor([0.0, 1.0, 0.0]),
                "best_foci": torch.tensor([0.0, 3.0, 4.0]),
                "middle_foci": torch.tensor([0.0, 2.0, 4.0]),
                "worst_foci": torch.tensor([0.0, 1.0, 4.0]),
            },
        ]:
            loss_best = loss_fn(test_case["best_foci"], test_case["target_t"])
            loss_middle = loss_fn(test_case["middle_foci"], test_case["target_t"])
            loss_worst = loss_fn(test_case["worst_foci"], test_case["target_t"])
            if loss_middle - loss_best >= loss_worst - loss_middle:
                errors.append(dedent(f"""
                    loss_fn failed to increase superlinearly wrt focus decrease
                    The middle-to-worst loss should exceed best-to-middle loss
                    target_t: {test_case["target_t"]}
                    best_foci: {test_case["best_foci"]}
                    loss for best_foci: {loss_best}
                    middle_foci: {test_case["middle_foci"]}
                    loss for middle_foci: {loss_middle}
                    worst_foci: {test_case["worst_foci"]}
                    loss for worst_foci: {loss_worst}
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))


    def test_distribution(self):
        """
        Ensure loss has a quadratic distribution and test specific values
        """
        errors = []
        for test_case in [
            {
                "input": {
                    "term_foci": torch.tensor([0.0, 1.0, 2.0]),
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                },
                "expected": 0.25,
            },
            {
                "input": {
                    "term_foci": torch.tensor([0.0, 1.0, 4.0]),
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                },
                "expected": 0.5625,
            },
            {
                "input": {
                    "term_foci": torch.tensor([0.0, 3.0, 4.0]),
                    "target_t": torch.tensor([0.0, 1.0, 0.0]),
                },
                "expected": 0.0625,
            },
        ]:
            actual = loss_fn(**test_case["input"])
            if actual != test_case["expected"]:
                errors.append(dedent(f"""
                    loss_fn failed.
                    term_foci: {test_case["input"]["term_foci"]}
                    target_t: {test_case["input"]["target_t"]}
                    Expected {test_case["expected"]} but got {actual}
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))
