"""
test_criteria.py

Tests for determining various criteria for
whether or not to process Arabic examples.

Not mocking arabic or pyarabic modules because they're fast.
"""


from textwrap import dedent
from unittest import skip

from test_examples_to_terms.util import TestUnit

from examples_to_terms.criteria import arabic_id_idx


class TestArabicIdIdx(TestUnit):
    """
    Test getting the index of a word id in a string
    """


    def setUp(self):
        """
        Set up the mocks
        """
        self.setup_mocks(
            namespace="examples_to_terms.criteria",
            to_mock=(
                "arabic_string_to_tokens",
                "logger",
            ),
        )


    def _test_arabic_id_idx(self, test_cases):
        """
        Generic test case method

        Example:

            Test cases:
                word1 has index i1 in string1, stripping tashkeel
                word2 has index i2 in string2, without stripping tashkeel

            Usage:
                self._test_arabic_id_idx([
                    {
                        "word": word1,
                        "string": string1,
                        "strip_tashkeel": True,
                        "expected": i1,
                    },
                    {
                        "word": word2,
                        "string": string2,
                        "strip_tashkeel": False,
                        "expected": i2,
                    },
                ])
        """
        errors = []
        for test_case in test_cases:
            word = test_case["word"]
            string = test_case["string"]
            strip_tashkeel = test_case["strip_tashkeel"]
            expected = test_case["expected"]
            actual = arabic_id_idx(word, string, strip_tashkeel=strip_tashkeel)
            if not actual == expected:
                errors.append(dedent(f"""
                    arabic_id_idx failed.
                    Tried: arabic_id_idx("{word}", "{string}", strip_tashkeel={strip_tashkeel})
                    Expected {expected} but got {actual}.
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))


    def test_default_strip_tashkeel(self):
        """
        Test that default behavior is to strip tashkeel

        The tokenizer strips tashkeel, so the result requires also stripping tashkeel on the word
        """
        self.mock["arabic_string_to_tokens"].side_effect = lambda string: {
            "انكب": "انكب",
        }[string]
        actual = arabic_id_idx("انكبّ", "انكبّ")
        self.assertEqual(actual, 1)


    def test_empty(self):
        """
        Test that empty strings yield not found
        """
        self._test_arabic_id_idx([
            {
                "word": "",
                "string": "",
                "strip_tashkeel": False,
                "expected": -1
            },
            {
                "word": "ﻖﻣﺭ",
                "string": "",
                "strip_tashkeel": False,
                "expected": -1
            },
            {
                "word": "",
                "string": "ﻖﻣﺭ",
                "strip_tashkeel": False,
                "expected": -1
            },
        ])


    def test_identity(self):
        """
        Test identity with and without shadda
        """
        self.mock["arabic_string_to_tokens"].side_effect = lambda string: {
            "قمر": "قمر",
            "انكبّ": "انكب",
            "انكب": "انكب",
        }[string]
        self._test_arabic_id_idx([
            {
                "word": "قمر",
                "string": "قمر",
                "strip_tashkeel": False,
                "expected": 1,
            },
            {
                "word": "انكبّ",
                "string": "انكبّ",
                "strip_tashkeel": False,
                "expected": -1,
            },
            {
                "word": "انكبّ",
                "string": "انكبّ",
                "strip_tashkeel": True,
                "expected": 1,
            },
        ])


    def test_root_in_morphed_word(self):
        """
        Test getting index for a root withing a morphed word
        """
        self.mock["arabic_string_to_tokens"].side_effect = lambda string: {
            "يطلع": "يطلع",
            "يشيل": "يشيل",
            "المواصلة": "ال+ مواصل +ة",
            "كلامه": "كلام +ه",
        }[string]
        self._test_arabic_id_idx([
            {
                "word": "طلع",
                "string": "يِطْلَع",
                "strip_tashkeel": True,
                "expected": 1,
            },
            {
                "word": "شال",
                "string": "يْشِيل",
                "strip_tashkeel": True,
                "expected": 1,
            },
            {
                "word": "مصلاوي",
                "string": "المَوَاْصْلَة",
                "strip_tashkeel": True,
                "expected": 2,
            },
            {
                "word": "شال",
                "string": "كَلَامَه",
                "strip_tashkeel": True,
                "expected": -1,
            },
        ])


    def test_root_in_sentence(self):
        """
        Test getting index for a root within a sentence
        """
        self.mock["arabic_string_to_tokens"].side_effect = lambda string: {
            "شغلة القواد طلعت قوادة": "شغل +ة ال+ قواد طلع +ت قواد +ة",
            "هاذي عروستنا": "هاذي عروستنا",
            "دامها أخت الرجال": "دام +ها أخت ال+ رجال",
            "يا حلاة عيونها": "يا حلا +ة عيون +ها",
        }[string]
        self._test_arabic_id_idx([
            {
                "word": "عروسة",
                "string": "هاذي عَروسَتْنَا",
                "strip_tashkeel": True,
                "expected": 2,
            },
            {
                "word": "حلاة",
                "string": "يا حَلَاة عِيُوْنْهَا",
                "strip_tashkeel": True,
                "expected": 2,
            },
            {
                "word": "أخت",
                "string": "دَامَهَا أَخْتِ الرِّجَّال",
                "strip_tashkeel": True,
                "expected": 3,
            },
            {
                "word": "دام",
                "string": "دَامَهَا أَخْتِ الرِّجَّال",
                "strip_tashkeel": True,
                "expected": 1,
            },
        ])


    @skip("Functionality not implemented")
    def test_distinct_morphs(self):
        """
        Test getting the index for a morphed word within a different morph of the word
        """
        raise NotImplementedError


    @skip("These tests fail because the tokenizer strips the shadda")
    def test_shadda(self):
        """
        Test finding the right word when the answer depends on shaddas
        """
        self.mock["arabic_string_to_tokens"].side_effect = lambda string: {
            "شغلة القواد طلعت قوادة": "شغل +ة ال+ قواد طلع +ت قواد +ة",
        }[string]
        self._test_arabic_id_idx([
            # Picks the 4th token because the tokenizer strips shaddas
            # Should pick the 7th token
            # Tokens: ['شغل', '+ة', 'ال+', 'قواد', 'طلع', '+ت', 'قواد', '+ة']
            {
                "word": "قوادة",
                "string": "شَغْلَةِ القُوَّاد طِلْعَت قْوَاْدَة",
                "strip_tashkeel": True,
                "expected": 7,
            },
        ])

    def test_non_classical_chars(self):
        """
        Test failure getting the index for words with non-classical chars

        The tokenizer can't handle those chars.
        """
        self.mock["arabic_string_to_tokens"].side_effect = lambda string: {
            "منو راح وچز إسمي": "منو راح و ز إسم +ي",
            "هاي السيپاية راح ما تتحمل الغراض إللي عليها": \
                "+ها +ي ال+ سي اية راح ما تتحمل ال+ غراض إللي علي +ها",
        }[string]
        self._test_arabic_id_idx([
            {
                "word": "چزّ",
                "string": "مِنُّو رَاح وچَزّ إِسْمِي",
                "strip_tashkeel": True,
                "expected": -1,
            },
            {
                "word": "سيپاية",
                "string": "هاي السِيْپاية راح ما تِتْحَمَّل الغَرَاْض إِللي عَلَيها",
                "strip_tashkeel": True,
                "expected": -1,
            },
        ])
