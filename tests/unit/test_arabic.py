"""
test_arabic.py

Tests for comparing Arabic strings and associated utils

Most of these functions do little more than wrap unicodedata tools,
so we won't mock unicodedata.
Instead, we want to ensure that those functions do what we need.
"""


from textwrap import dedent

from test_examples_to_terms.util import TestUnit

from examples_to_terms import arabic


class TestEquals(TestUnit):
    """
    Test assessing equality for Arabic strings
    """

    def _test_equals(self, test_cases):
        """
        Generic test case method

        Example:

            Test cases:
                string1 == string2
                string3 != string4

            Usage:
                self._test_equals([
                    {"lhs": string1, "rhs": string2, "expected": True},
                    {"lhs": string3, "rhs": string4, "expected": False},
                ])
        """
        errors = []
        for test_case in test_cases:
            lhs, rhs, expected = test_case["lhs"], test_case["rhs"], test_case["expected"]
            actual = arabic.equals(lhs, rhs)
            if not actual == expected:
                errors.append(dedent(f"""
                    arabic.equals failed.
                    Tried: arabic.equals("{lhs}", "{rhs}")
                    Expected {expected} but got {actual}.
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))


    def test_empty(self):
        """
        Test that two empty strings are equal
        """
        self._test_equals([
            {"lhs": "", "rhs": "", "expected": True},
        ])


    def test_identity(self):
        """
        Test that things equal themselves
        """
        self._test_equals([
            {"lhs": "ﺓ", "rhs": "ﺓ", "expected": True},
            {"lhs": "ﺖﻨﻔّﻫ", "rhs": "ﺖﻨﻔّﻫ", "expected": True},
        ])


    def test_symmetry(self):
        """
        Test that symmetry holds for a typical case
        """
        self._test_equals([
            {"lhs": "ﻛ", "rhs": "ﻚ", "expected": True},
            {"lhs": "ﻚ", "rhs": "ﻛ", "expected": True},
        ])


    def test_initial_and_terminal(self):
        """
        Test that corresponding initial and terminal chars are equal
        """
        self._test_equals([
            {"lhs": "ﺐ", "rhs": "ﺑ", "expected": True},
            {"lhs": "ﺖ", "rhs": "ﺗ", "expected": True},
            #{"lhs": "", "rhs": "ﺟ", "expected": True},
            {"lhs": "ﺢ", "rhs": "ﺣ", "expected": True},
            {"lhs": "ﺦ", "rhs": "ﺧ", "expected": True},
            {"lhs": "ﺲ", "rhs": "ﺳ", "expected": True},
            {"lhs": "ﺶ", "rhs": "ﺷ", "expected": True},
            {"lhs": "ﺺ", "rhs": "ﺻ", "expected": True},
            #{"lhs": "", "rhs": "ﺿ", "expected": True},
            {"lhs": "ﻊ", "rhs": "ﻋ", "expected": True},
            {"lhs": "ﻎ", "rhs": "ﻏ", "expected": True},
            {"lhs": "ﻒ", "rhs": "ﻓ", "expected": True},
            {"lhs": "ﻖ", "rhs": "ﻗ", "expected": True},
            {"lhs": "ﻚ", "rhs": "ﻛ", "expected": True},
            {"lhs": "ﻞ", "rhs": "ﻟ", "expected": True},
            {"lhs": "ﻢ", "rhs": "ﻣ", "expected": True},
            {"lhs": "ﻦ", "rhs": "ﻧ", "expected": True},
            #{"lhs": "", "rhs": "ﻫ", "expected": True},
            {"lhs": "ﻲ", "rhs": "ﻳ", "expected": True},
        ])


    def test_initial_and_medial(self):
        """
        Test that corresponding initial and medial chars are equal
        """
        self._test_equals([
            {"lhs": "ﺒ", "rhs": "ﺑ", "expected": True},
            {"lhs": "ﺘ", "rhs": "ﺗ", "expected": True},
            {"lhs": "ﺠ", "rhs": "ﺟ", "expected": True},
            {"lhs": "ﺤ", "rhs": "ﺣ", "expected": True},
            {"lhs": "ﺨ", "rhs": "ﺧ", "expected": True},
            {"lhs": "ﻂ", "rhs": "ﻁ", "expected": True},
            #{"lhs": "", "rhs": "ﻇ", "expected": True},
            {"lhs": "ﺴ", "rhs": "ﺳ", "expected": True},
            {"lhs": "ﺸ", "rhs": "ﺷ", "expected": True},
            {"lhs": "ﺼ", "rhs": "ﺻ", "expected": True},
            #{"lhs": "", "rhs": "ﺿ", "expected": True},
            {"lhs": "ﻌ", "rhs": "ﻋ", "expected": True},
            {"lhs": "ﻐ", "rhs": "ﻏ", "expected": True},
            {"lhs": "ﻔ", "rhs": "ﻓ", "expected": True},
            {"lhs": "ﻘ", "rhs": "ﻗ", "expected": True},
            {"lhs": "ﻜ", "rhs": "ﻛ", "expected": True},
            {"lhs": "ﻠ", "rhs": "ﻟ", "expected": True},
            {"lhs": "ﻤ", "rhs": "ﻣ", "expected": True},
            {"lhs": "ﻨ", "rhs": "ﻧ", "expected": True},
            {"lhs": "ﻬ", "rhs": "ﻫ", "expected": True},
            {"lhs": "ﻴ", "rhs": "ﻳ", "expected": True},
        ])


    def test_initial_and_isolated(self):
        """
        Test that corresponding initial and isolated chars are equal
        """
        self._test_equals([
            {"lhs": "ﺏ", "rhs": "ﺑ", "expected": True},
            {"lhs": "ﺕ", "rhs": "ﺗ", "expected": True},
            {"lhs": "ﺝ", "rhs": "ﺟ", "expected": True},
            {"lhs": "ﺡ", "rhs": "ﺣ", "expected": True},
            {"lhs": "ﺥ", "rhs": "ﺧ", "expected": True},
            {"lhs": "ﺱ", "rhs": "ﺳ", "expected": True},
            {"lhs": "ﺵ", "rhs": "ﺷ", "expected": True},
            {"lhs": "ﺺ", "rhs": "ﺻ", "expected": True},
            {"lhs": "ﻉ", "rhs": "ﻋ", "expected": True},
            {"lhs": "ﻍ", "rhs": "ﻏ", "expected": True},
            #{"lhs": "", "rhs": "ﻛ", "expected": True},
            {"lhs": "ﻑ", "rhs": "ﻓ", "expected": True},
            {"lhs": "ﻕ", "rhs": "ﻗ", "expected": True},
            {"lhs": "ﻙ", "rhs": "ﻛ", "expected": True},
            {"lhs": "ﻥ", "rhs": "ﻧ", "expected": True},
            {"lhs": "ﻩ", "rhs": "ﻫ", "expected": True},
            {"lhs": "ﻱ", "rhs": "ﻳ", "expected": True},
        ])


    def test_terminal_and_isolated(self):
        """
        Test that corresponding terminal and isolated chars are equal
        """
        self._test_equals([
            #{"lhs": "ﺄ", "rhs": "", "expected": True},
            {"lhs": "ﺎ", "rhs": "ﺍ", "expected": True},
            #{"lhs": "", "rhs": "ﺩ", "expected": True},
            {"lhs": "ﺬ", "rhs": "ﺫ", "expected": True},
            {"lhs": "ﺮ", "rhs": "ﺭ", "expected": True},
            #{"lhs": "", "rhs": "ﺯ", "expected": True},
            {"lhs": "ﺔ", "rhs": "ﺓ", "expected": True},
            #{"lhs": "", "rhs": "ﻩ", "expected": True},
            {"lhs": "ﻮ", "rhs": "ﻭ", "expected": True},
            #{"lhs": "", "rhs": "ﻯ", "expected": True},
            {"lhs": "ﻼ", "rhs": "ﻻ", "expected": True},
        ])


    def test_special(self):
        """
        Test equality for special pairs
        """
        self._test_equals([
            {"lhs": "ﻻ", "rhs": "ﻟﺎ", "expected": True},

            # The following look the same but have different unicode representations
            # chr(65171) equals chr(1577)
            {"lhs": "ﺓ", "rhs": "ة", "expected": True},
        ])


    def test_non_classical_chars(self):
        """
        Test that corresponding non-classical characters are equal
        """
        self._test_equals([
            {"lhs": "ﭗ", "rhs": "ﭘ", "expected": True},
            {"lhs": "ﭖ", "rhs": "ﭘ", "expected": True},
            {"lhs": "ﭺ", "rhs": "ﭼ", "expected": True},
            {"lhs": "ﭻ", "rhs": "ﭼ", "expected": True},
        ])


    def test_shadda_inequality(self):
        """
        Test that characters are not equal when exactly one has a shadda
        """
        self._test_equals([
            {"lhs": "ﻜ", "rhs": "ﻜّ", "expected": False},
        ])


    def test_tah_marbootah_inequality(self):
        """
        Test inequality ﺕ != ﺓ
        """
        self._test_equals([
            {"lhs": "ﺕ", "rhs": "ﺓ", "expected": False},
        ])


    def test_type_error(self):
        """
        Test error for non-string
        """
        self.assertRaises(TypeError, arabic.equals, *(1, "ﻜ"))


class TestStartsWith(TestUnit):
    """
    Test determining if an Arabic string starts with another
    """


    def _test_startswith(self, test_cases):
        """
        Generic test case method

        Example:

            Test cases:
                string1 starts with string2
                string3 doesn't start with string4

            Usage:
                self._test_startswith([
                    {"string": string1, "candidate": string2, "expected": True},
                    {"string": string3, "candidate": string4, "expected": False},
                ])
        """
        errors = []
        for test_case in test_cases:
            string = test_case["string"]
            candidate = test_case["candidate"]
            expected = test_case["expected"]
            actual = arabic.startswith(string, candidate)
            if not actual == expected:
                errors.append(dedent(f"""
                    arabic.startswith failed.
                    Tried: arabic.startswith("{string}", "{candidate}")
                    Expected {expected} but got {actual}.
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))




    def test_empty_superset(self):
        """
        Test when larger string is empty
        """
        self._test_startswith([
            {"string": "", "candidate": "", "expected": True},
            {"string": "", "candidate": "باق", "expected": False},
        ])


    def test_empty_subset(self):
        """
        Test when the candidate is empty
        """
        self._test_startswith([
            {"string": "باق", "candidate": "", "expected": True},
        ])


    def test_identity(self):
        """
        Test that strings start with themselves
        """
        self._test_startswith([
            {"string": "باق", "candidate": "باق", "expected": True},
            {"string": "الأَكُو وْالمَاْكُو", "candidate": "الأَكُو وْالمَاْكُو", "expected": True},
        ])


    def test_initial_substring(self):
        """
        Test when the candidate is an initial substring
        """
        self._test_startswith([
            {"string": "باق", "candidate": "ب", "expected": True},
            {"string": "باق", "candidate": "ﻘ", "expected": False},
            {"string": "الأَكُو وْالمَاْكُو وْشِلَع", "candidate": "الأَكُو وْا", "expected": True},
        ])


    def test_noninitial_substring(self):
        """
        Test when the candidate is a substring, but not an initial substring
        """
        self._test_startswith([
            {"string": "باق", "candidate": "اق", "expected": False},
            {"string": "الأَكُو وْالمَاْكُو", "candidate": "وْالمَاْكُو", "expected": False},
            {"string": "الأَكُو وْالمَاْكُو", "candidate": "كُو وْالمَاْكُ", "expected": False},
        ])


    def test_nonsubstring(self):
        """
        Test when the candidate is not a substring
        """
        self._test_startswith([
            {"string": "باق", "candidate": "باقق", "expected": False},
            {"string": "باق", "candidate": "ص", "expected": False},
            {"string": "الأَكُو وْالمَاْكُو", "candidate": "الأَكُو وْلمَاْكُو", "expected": False},
        ])


    def test_exterior_spaces(self):
        """
        Test that exterior spaces affect the result
        """
        self._test_startswith([
            {"string": "با ق", "candidate": "ب", "expected": True},
            {"string": " باق", "candidate": "ب", "expected": False},
            {"string": "باق", "candidate": " ب", "expected": False},
            {"string": "باق", "candidate": "ب ", "expected": False},
        ])


    def test_special_chars(self):
        """
        Test that special characters work as expected
        """
        self._test_startswith([
            {"string": "ﻻ", "candidate": "ﻟﺎ", "expected": True},
            {"string": "ﻟﺎ", "candidate": "ﻻ", "expected": True},
        ])


    def test_tah_marbootah(self):
        """
        Test that the tah marbootah doesn't match with tah
        """
        self._test_startswith([
            {"string": "ﻉﺭﻮﺴﺘﻧﺍ", "candidate": "ﻉﺭﻮﺳﺓ", "expected": False},
        ])


    def test_type_error(self):
        """
        Test error for non-string
        """
        self.assertRaises(TypeError, arabic.startswith, *(1, "ﻜ"))


class TestContains(TestUnit):
    """
    Test determining if an Arabic string contains another
    """


    def _test_contains(self, test_cases):
        """
        Generic test case method

        Example:

            Test cases:
                string1 contains string2
                string3 doesn't contain string4

            Usage:
                self._test_contains([
                    {"string": string1, "candidate": string2, "expected": True},
                    {"string": string3, "candidate": string4, "expected": False},
                ])
        """
        errors = []
        for test_case in test_cases:
            string = test_case["string"]
            candidate = test_case["candidate"]
            expected = test_case["expected"]
            actual = arabic.contains(string, candidate)
            if not actual == expected:
                errors.append(dedent(f"""
                    arabic.contains failed.
                    Tried: arabic.contains("{string}", "{candidate}")
                    Expected {expected} but got {actual}.
                """))
        self.assertListEqual(errors, [], "\n" + "".join(errors))


    def test_empty_superset(self):
        """
        Test when larger string is empty
        """
        self._test_contains([
            {"string": "", "candidate": "", "expected": True},
            {"string": "", "candidate": "باق", "expected": False},
        ])


    def test_empty_subset(self):
        """
        Test when the candidate is empty
        """
        self._test_contains([
            {"string": "باق", "candidate": "", "expected": True},
        ])


    def test_identity(self):
        """
        Test that strings contain themselves
        """
        self._test_contains([
            {"string": "باق", "candidate": "باق", "expected": True},
            {"string": "الأَكُو وْالمَاْكُو", "candidate": "الأَكُو وْالمَاْكُو", "expected": True},
        ])


    def test_substring(self):
        """
        Test when the candidate is a substring
        """
        self._test_contains([
            {"string": "باق", "candidate": "اق", "expected": True},
            {"string": "الأَكُو وْالمَاْكُو", "candidate": "وْالمَاْكُو", "expected": True},
            {"string": "الأَكُو وْالمَاْكُو", "candidate": "كُو وْالمَاْكُ", "expected": True},
        ])


    def test_nonsubstring(self):
        """
        Test when the candidate is not a substring
        """
        self._test_contains([
            {"string": "باق", "candidate": "باقق", "expected": False},
            {"string": "باق", "candidate": "ص", "expected": False},
            {"string": "الأَكُو وْالمَاْكُو", "candidate": "الأَكُو وْلمَاْكُو", "expected": False},
        ])


    def test_exterior_spaces(self):
        """
        Test that exterior spaces affect the result
        """
        self._test_contains([
            {"string": "با ق", "candidate": "ب", "expected": True},
            {"string": " باق", "candidate": "ب", "expected": True},
            {"string": "باق", "candidate": " ب", "expected": False},
            {"string": "باق", "candidate": "ب ", "expected": False},
        ])


    def test_special_chars(self):
        """
        Test that special characters work as expected
        """
        self._test_contains([
            {"string": "ﻻ", "candidate": "ﻟﺎ", "expected": True},
            {"string": "ﻟﺎ", "candidate": "ﻻ", "expected": True},
        ])


    def test_tah_marbootah(self):
        """
        Test that the tah marbootah doesn't match with tah
        """
        self._test_contains([
            {"string": "ﻉﺭﻮﺴﺘﻧﺍ", "candidate": "ﻉﺭﻮﺳﺓ", "expected": False},
        ])


    def test_type_error(self):
        """
        Test error for non-string
        """
        self.assertRaises(TypeError, arabic.contains, *(1, "ﻜ"))
