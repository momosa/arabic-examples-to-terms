#!/usr/bin/env python
#pylint: disable=missing-module-docstring

import argparse
import logging
import csv
import json
import re
import sys
from pathlib import Path

from examples_to_terms.config import (
    DEFAULT_LOG_LEVEL,
    DIALECTS,
    DEFAULT_DIALECT,
    CSV_DELIMITER,
    OUTPUT_FORMATS,
    DEFAULT_OUTPUT_FORMAT,
    WEIGHTS,
)

from examples_to_terms.dictionary_term import DictionaryTerm
from examples_to_terms.arabic_term_result import ArabicTermResult
from examples_to_terms.result import Result

from examples_to_terms.indata import get_examples
from examples_to_terms.text_processing import arabic_string_to_tokens

logger = logging.getLogger(__name__)


DEFAULT_EXAMPLES_FILE = Path(__file__).parents[1] / "data" / "sample_examples.csv"
DEFAULT_DICTIONARY_FILE = Path(__file__).parents[1] / "data" / "standardized_input.csv"
LOG_FILE = Path(__file__).parents[1] / "terms_from_examples.log"

DESCRIPTION = f"""Apply the model to a set of examples.

The input file should use CSV delimiter '{CSV_DELIMITER}'.
The first column should contain examples.
See {DEFAULT_EXAMPLES_FILE} for examples.

Consider redirecting stdout to a file if you're assessing a lot of examples:

  terms_from_examples.py > /tmp/output.txt
"""

EPILOG = """Examples:

  terms_from_examples.py
  terms_from_examples.py --log INFO
  terms_from_examples.py \\
        --weights \\
            -0.2045 -0.4501 0.0292 -0.0311 -0.4399 -0.4113 \\
            0.2034 1.0451 0.7457 0.3789 -0.3856 0.2451 \\
        --maxexamples 10
  terms_from_examples.py --weights $(sed 's/|/ /g' data/weights.csv)
"""

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=DESCRIPTION,
    epilog=EPILOG,
)

parser.add_argument(
    "-l",
    "--log",
    type=str,
    default=DEFAULT_LOG_LEVEL,
    help="Designate logging level as DEBUG, INFO, WARN, ERROR, or CRITICAL",
)

parser.add_argument(
    "-w",
    "--weights",
    action="extend",
    type=float,
    nargs="*",
    help=f"""
        The weights used for attention layers.
        There must be 12.
        default={WEIGHTS}
    """,
)

parser.add_argument(
    "-m",
    "--maxexamples",
    type=int,
    default=-1,
    help=f"""
        The maximum number of examples to process from the data set.
        Use a non-positive number to process all examples from the data set.
        default={-1}
    """,
)

parser.add_argument(
    "-e",
    "--examples",
    type=str,
    default=DEFAULT_EXAMPLES_FILE,
    help=f"""
        Specify the input file containing examples.
        default={DEFAULT_EXAMPLES_FILE}
    """,
)

parser.add_argument(
    "--dictionary",
    type=str,
    default=DEFAULT_DICTIONARY_FILE,
    help=f"""
        Specify the dictionary file.
        default={DEFAULT_DICTIONARY_FILE}
    """,
)

parser.add_argument(
    "--dialect",
    default=DEFAULT_DIALECT,
    help=f"""
        Specify the dialect.
        Available dialects: {DIALECTS}.
        default={DEFAULT_DIALECT}
    """,
)

parser.add_argument(
    "--output",
    default=DEFAULT_OUTPUT_FORMAT,
    help=f"""
        Specify the output format.
        Available formats: {OUTPUT_FORMATS}
        default={DEFAULT_OUTPUT_FORMAT}
    """,
)

def get_dictionary(dictionary_file, dialect):
    """
    Get the dictionary for the given dialect

    :param str dictionary_file: The filename for the dictionary
    :param str dialect: The dialect that we want to look at
    :return: Dictionary data
    :rtype: list
    """
    terms = []
    logger.info("Reading dictionary from %s", dictionary_file)
    try:
        with open(dictionary_file, "r", encoding="utf-8") as dict_file:
            data_arr = list(csv.reader(dict_file, delimiter=CSV_DELIMITER))
    except UnicodeDecodeError:
        logger.critical(
            "Dictionary file '%s' is not in utf-8 format",
            dictionary_file,
        )
        raise
    logger.info("Finished reading dictionary from file")

    criteria = (
        lambda term: term.is_target_recognized(),
        lambda term: term.is_word_in_example(),
    )

    logger.info("Isolating '%s' dialect in the dictionary data ...", dialect)
    data_arr = [r for r in data_arr if r[0] == dialect]

    logger.info("Creating dictionary from data ...")
    for row in data_arr:
        try:
            term_kwargs = {
                "word": row[1],
                "definition": row[2],
                "arabic_example": row[3],
                "english_example": row[4],
            }
            terms.append(DictionaryTerm(**term_kwargs))
        except IndexError:
            logger.warning("Missing data for dictionary row: %s", row)
        except ValueError:
            logger.warning("Bad user input for dictionary row: %s", row)

    logger.info(
        "The tokenizer recognizes %s terms out of %s",
        len([t.is_target_recognized() for t in terms]),
        len(terms),
    )
    logger.info(
        "Recognized terms in their Arabic examples: %s out of %s",
        len([t.is_word_in_example() for t in terms]),
        len(terms),
    )
    for term in terms:
        if term.is_target_recognized() and not term.is_word_in_example():
            logger.info(
                "Term %s is recognized but not in arabic example %s",
                term.word,
                term.arabic_example,
            )
            logger.info(
                "Example tokens: %s",
                arabic_string_to_tokens(term.arabic_example).split(" ")
            )

    terms = [
        term for term in terms
        if all(criterion(term) for criterion in criteria)
    ]

    return terms


def get_examples_from_file(examples_file, max_examples):
    """
    Get the examples for the the given file

    :param str examples_file: The filename containing examples.
    :param int max_examples: The maximum number of examples to use
    :return: Examples data
    :rtype: list
    """
    egs = []

    logger.info("Reading examples from %s", examples_file)
    try:
        with open(examples_file, "r", encoding="utf-8") as eg_file:
            data_arr = list(csv.reader(eg_file, delimiter=CSV_DELIMITER))
    except UnicodeDecodeError:
        logger.critical(
            "Examples file '%s' is not in utf-8 format",
            examples_file,
        )
        raise
    logger.info("Finished reading examples from file")

    for row in data_arr:
        try:
            preprocessed_arabic = re.split(r"\[[A-Za-z-_]+\]", row[0])[0]
        except IndexError:
            logger.warning("Missing example in row: '%s'", row)
            preprocessed_arabic = None
        english_example, arabic_example = get_examples(row[0])
        if arabic_example and english_example:
            egs.append({
                "preprocessed_arabic": preprocessed_arabic,
                "arabic_example": arabic_example,
                "english_example": english_example,
            })
        else:
            logger.warning("Unable to parse example: '%s'", row[0])
        if max_examples > 0 and len(egs) == max_examples:
            break

    return egs


if __name__ == '__main__':
    args = parser.parse_args()
    logging.basicConfig(
        filename=LOG_FILE,
        encoding="utf-8",
        filemode="w",
        level=getattr(logging, args.log.upper(), DEFAULT_LOG_LEVEL),
    )
    if args.output not in OUTPUT_FORMATS:
        raise ValueError(f"Unknown output format '{args.output}'. See --help")
    if args.output == "txt":
        print(f"Generating log file '{LOG_FILE}' in the project dir")
    csv.field_size_limit(sys.maxsize)

    dictionary = get_dictionary(args.dictionary, args.dialect)
    examples = get_examples_from_file(args.examples, args.maxexamples)

    weights = args.weights if args.weights else WEIGHTS[:]
    if len(weights) != 12:
        raise ValueError("If you specify weights, there must be exactly 12.")

    ArabicTermResult.set_weights(weights)
    Result.set_dictionary(dictionary)
    results = []
    for idx, example in enumerate(examples):
        if idx % 100 == 0 and args.output == "txt":
            print(f"Assessing example {idx+1} of {len(examples)}.")
        try:
            result_a = ArabicTermResult(
                example["arabic_example"],
                preprocessed_arabic=example["preprocessed_arabic"],
            )
            result = Result(result_a, example["english_example"])
            results.append(result)
        except ValueError:
            logger.warning("Failed to determine key terms for example '%s'", example)

    if args.output == "txt":
        print("\n")
        for r in results:
            print(r.as_string())
            print("\n")
    elif args.output == "csv":
        print(Result.csv_header(CSV_DELIMITER))
        for r in results:
            print(r.as_csv_string(CSV_DELIMITER))
    elif args.output == "json":
        print(json.dumps(
            [dict(r) for r in results],
            indent=4,
            ensure_ascii=False,
        ))
    else:
        raise ValueError(f"Unknown output format '{args.output}'. See --help")
