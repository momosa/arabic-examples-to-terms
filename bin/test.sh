#!/bin/sh -e


usage() {
cat << END_HELP
USAGE

    test.sh [-h|--help|help|unit|lint|integration|all]

EXAMPLES

    # Print this message
    test.sh help

    # Run all tests
    test.sh
    test.sh all

    # Run unit tests
    test.sh unit

    # Run lint tests
    test.sh lint

    # Run integration tests
    test.sh integration

    # Run unit and lint tests
    test.sh unit lint

END_HELP
}

TESTS="${@}"
INITIAL_DIR=${PWD}

flop() {
    cd "${INITIAL_DIR}" > /dev/null
    exit 1
}

for t in ${TESTS}; do
    if [ ${t} = '-h' ] || [ ${t} = '--help' ] || [ ${t} = 'help' ]; then
        usage
        exit 0
    fi
done

if [ -z "${TESTS}" ] || [ "${TESTS}" = 'all' ]; then
    TESTS='unit lint integration'
fi

for t in ${TESTS}; do
    if [ ${t} != 'unit' ] && [ ${t} != 'lint' ] && [ ${t} != 'integration' ]; then
        echo "Unrecognized option '${t}'. Try 'test.sh help'." 1>&2
        exit 1
    fi
done

cd "$(dirname ${0})/.." > /dev/null
for t in ${TESTS}; do
    if [ ${t} = 'unit' ]; then
        pytest tests/unit || flop
    elif [ ${t} = 'lint' ]; then
        pylint --rcfile .pylintrc src/examples_to_terms || flop
        pylint --rcfile bin/.pylintrc bin || flop
        pylint --rcfile tests/.pylintrc tests || flop
    elif [ ${t} = 'integration' ]; then
        dictionary="$(dirname "${0}")/../data/standardized_input_test.csv"
        bin/standardize_input.py --dryrun --log INFO || flop
        bin/train.py --dryrun --samplesize 10 --epochs 10 --dialects Iraqi --log INFO || flop
        bin/terms_from_examples.py --dictionary "${dictionary}" --maxexamples 10 --log INFO || flop
        bin/assess_terms_from_examples.py --maxexamples 10 --log INFO || flop
    fi
done
cd "${INITIAL_DIR}" > /dev/null
