#!/usr/bin/env python
#pylint: disable=missing-module-docstring

import argparse
import logging
from pathlib import Path

from examples_to_terms.indata import get_input_data

from examples_to_terms.config import (
    DEFAULT_LOG_LEVEL,
    CSV_DELIMITER,
    INPUT_COLUMNS,
)

logger = logging.getLogger(__name__)

INPUT_DIR = Path(__file__).parents[1] / "data" / "input"
STANDARDIZED_INPUT_FILE = Path(__file__).parents[1] / "data" / "standardized_input.csv"
LOG_FILE = Path(__file__).parents[1] / "standardize_input.log"

DESCRIPTION = f"""Get the input data, clean it up, and store it in a new csv.

Data is read from {INPUT_DIR}
Use CSV delimiter '{CSV_DELIMITER}'.
Expected columns: {INPUT_COLUMNS}

Standardized data is output to:
{STANDARDIZED_INPUT_FILE}
"""

EPILOG = f"""Examples:

  standardize_input.py
  standardize_input.py --log INFO
  standardize_input.py --dryrun

For extra funzies, you can watch the logs:

  tail -f {LOG_FILE}
"""

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=DESCRIPTION,
    epilog=EPILOG,
)

parser.add_argument(
    "-l",
    "--log",
    type=str,
    default=DEFAULT_LOG_LEVEL,
    help=f"""
        Designate logging level as DEBUG, INFO, WARN, ERROR, or CRITICAL.
        Logs will be stored in {LOG_FILE}.
    """,
)

parser.add_argument(
    "-n",
    "--dryrun",
    default=False,
    action="store_true",
    help="""
        Don't actually write the output file.
    """,
)

if __name__ == '__main__':
    args = parser.parse_args()
    logging.basicConfig(
        filename=LOG_FILE,
        encoding="utf-8",
        filemode="w",
        level=getattr(logging, args.log.upper(), DEFAULT_LOG_LEVEL),
    )
    print(f"Generating log file '{LOG_FILE}' in the project dir")

    logger.info("Getting data from raw input files")
    data_arr = get_input_data(INPUT_DIR, args.dryrun)
    logger.info("Finished getting data from raw input files")

    logger.info("Generating standartized csv input")
    csv_rows = [CSV_DELIMITER.join(row) for row in data_arr]
    DATA_CSV = "\n".join(csv_rows)
    logger.info("Finished generating standartized csv input")

    if not args.dryrun:
        logger.info("Writing standardized input to %s", STANDARDIZED_INPUT_FILE)
        with open(STANDARDIZED_INPUT_FILE, "w", encoding="utf-8") as standardized_file:
            standardized_file.write(DATA_CSV)
        logger.info("Finished writing standardized input to %s", STANDARDIZED_INPUT_FILE)
