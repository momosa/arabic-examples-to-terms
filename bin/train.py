#!/usr/bin/env python
#pylint: disable=missing-module-docstring

import argparse
import logging
import csv
import sys
import random
from pathlib import Path
import torch

from examples_to_terms.training import training_loop

from examples_to_terms.criteria import (
    is_dialect,
    is_word_recognized,
    is_word_in_example,
)

from examples_to_terms.config import (
    DEFAULT_LOG_LEVEL,
    CSV_DELIMITER,
    N_EPOCHS,
    LEARNING_RATE,
    SAMPLE_SIZE,
    DIALECTS,
)

logger = logging.getLogger(__name__)

# These columns are designated in indata.py when defining standardized_row
COLUMN = {
    "dialect": 0,
    "word": 1,
    "meaning": 2,
    "arabic_example": 3,
    "english_example": 4,
}

STANDARDIZED_INPUT_FILE = Path(__file__).parents[1] / "data" / "standardized_input.csv"
WEIGHTS_FILE = Path(__file__).parents[1] / "data" / "weights.csv"
LOG_FILE = Path(__file__).parents[1] / "train.log"

DESCRIPTION = f"""Train the model to see which weights work best.

Data is read from {STANDARDIZED_INPUT_FILE}
Expected columns: {COLUMN}

Weights are outputted to:
{WEIGHTS_FILE}
Weights are also given in the logs at the INFO level.
"""

EPILOG = f"""Examples:

  train.py
  train.py --log INFO
  train.py \\
    --samplesize 1000 \\
    --epoch 300 \\
    --dialects Iraqi Levantine
  train.py \\
    --samplesize 10 \\
    --epoch 10 \\
    --dialects Iraqi \\
    --dryrun

For extra funzies, you can watch the logs:

  tail -f {LOG_FILE}
"""

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=DESCRIPTION,
    epilog=EPILOG,
)

parser.add_argument(
    "-l",
    "--log",
    type=str,
    default=DEFAULT_LOG_LEVEL,
    help=f"""
        Designate logging level as DEBUG, INFO, WARN, ERROR, or CRITICAL.
        default={DEFAULT_LOG_LEVEL}
    """,
)

parser.add_argument(
    "-e",
    "--epochs",
    type=int,
    default=N_EPOCHS,
    help=f"""
        The number of epochs, or training cycles.
        default={N_EPOCHS}
    """,
)

parser.add_argument(
    "-d",
    "--dialects",
    type=str,
    nargs="*",
    action="extend",
    help="""
        The dialect used for training.
        All dialects are used by default.
    """,
)

parser.add_argument(
    "-s",
    "--samplesize",
    type=int,
    default=SAMPLE_SIZE,
    help=f"""
        The sample size used for training.
        A non-positive value (0 or less) will use the entire data set.
        default={SAMPLE_SIZE}
    """,
)

parser.add_argument(
    "-n",
    "--dryrun",
    default=False,
    action="store_true",
    help="""
        Don't actually write the output file.
    """,
)

if __name__ == '__main__':
    args = parser.parse_args()
    logging.basicConfig(
        filename=LOG_FILE,
        encoding="utf-8",
        filemode="w",
        level=getattr(logging, args.log.upper(), DEFAULT_LOG_LEVEL),
    )
    print(f"Generating log file '{LOG_FILE}' in the project dir")
    csv.field_size_limit(sys.maxsize)

    dialects = args.dialects if args.dialects else DIALECTS[:]
    if all(d in DIALECTS for d in dialects):
        logger.info("Using dialects %s", dialects)
    else:
        raise ValueError(f"Unrecognized dialect in '{dialects}'")

    criteria = (
        lambda row: any(is_dialect(row[COLUMN["dialect"]], d) for d in dialects),
        lambda row: is_word_recognized(row[COLUMN["word"]]),
        lambda row: is_word_in_example(row[COLUMN["word"]], row[COLUMN["arabic_example"]]),
    )

    data_arr = []
    logger.info("Reading standardized data from %s", STANDARDIZED_INPUT_FILE)
    with open(STANDARDIZED_INPUT_FILE, "r", encoding="utf-8") as standardized_input:
        data_arr = list(csv.reader(standardized_input, delimiter=CSV_DELIMITER))
    data_arr = [
        row for row in data_arr
        if all(criterion(row) for criterion in criteria)
    ]
    logger.info("Finished reading standardized data")

    arabic_examples = [row[COLUMN["arabic_example"]] for row in data_arr]
    target_terms = [row[COLUMN["word"]] for row in data_arr]

    # Designating sample size here instead of training to
    # ensure consistent loss evaluation.
    if args.samplesize > 0:
        logger.info("Using sample size {args.samplesize}")
        indexes = random.sample(range(len(arabic_examples)), args.samplesize)
        arabic_examples = [arabic_examples[i] for i in indexes]
        target_terms = [target_terms[i] for i in indexes]

    logger.info("Training model")
    weights = torch.tensor([0.5 for _ in range(12)], requires_grad=True)
    logger.debug("Initial weights are %s", weights)
    learning_rate = LEARNING_RATE
    logger.debug("Initial learning_rate is %s", learning_rate)
    weights = training_loop(
        arabic_examples,
        target_terms,
        n_epochs=args.epochs,
        weights=weights,
        optimizer=torch.optim.Adam([weights], lr=learning_rate),
    )
    logger.info("Finished training model")

    if not args.dryrun:
        print(f"Writing weights to {WEIGHTS_FILE}")
        logger.info("Writing weights to %s", WEIGHTS_FILE)
        weights_string = CSV_DELIMITER.join([str(w) for w in weights.tolist()])
        with open(WEIGHTS_FILE, "w", encoding="utf-8") as weights_file:
            weights_file.write(weights_string)
        logger.info("Finished writing weights to file")
