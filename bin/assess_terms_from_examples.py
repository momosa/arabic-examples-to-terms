#!/usr/bin/env python
#pylint: disable=missing-module-docstring

from textwrap import dedent
import argparse
import logging
import csv
import json
import re
import sys
from pathlib import Path

from examples_to_terms import arabic

from examples_to_terms.config import (
    DEFAULT_LOG_LEVEL,
    CSV_DELIMITER,
    OUTPUT_FORMATS,
    DEFAULT_OUTPUT_FORMAT,
    WEIGHTS,
)

from examples_to_terms.dictionary_term import DictionaryTerm
from examples_to_terms.arabic_term_result import ArabicTermResult
from examples_to_terms.result import Result

from examples_to_terms.indata import get_examples
from examples_to_terms.text_processing import arabic_string_to_tokens

logger = logging.getLogger(__name__)


DEFAULT_EXAMPLES_FILE = Path(__file__).parents[1] / "data" / "assessment_examples.csv"
LOG_FILE = Path(__file__).parents[1] / "assess_terms_from_examples.log"

DESCRIPTION = f"""Apply the model to a set of examples and assess the mode's accuracy.

The input file should use CSV delimiter '{CSV_DELIMITER}'.
The first column should contain examples.
The second column should contain the target term (word).
See {DEFAULT_EXAMPLES_FILE} for examples.

The target term must be in the example and
the model's dictionary needs to contain the target term.
Other examples will be skipped.

Consider redirecting stdout to a file if you're assessing a lot of examples:

  assess_terms_from_examples.py > /tmp/output.txt
"""

EPILOG = """Examples:

  assess_terms_from_examples.py
  assess_terms_from_examples.py --log INFO
  assess_terms_from_examples.py \\
        --weights \\
            -0.2045 -0.4501 0.0292 -0.0311 -0.4399 -0.4113 \\
            0.2034 1.0451 0.7457 0.3789 -0.3856 0.2451 \\
        --maxexamples 10
  assess_terms_from_examples.py --weights $(sed 's/|/ /g' data/weights.csv)
"""

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=DESCRIPTION,
    epilog=EPILOG,
)

parser.add_argument(
    "-l",
    "--log",
    type=str,
    default=DEFAULT_LOG_LEVEL,
    help="Designate logging level as DEBUG, INFO, WARN, ERROR, or CRITICAL",
)

parser.add_argument(
    "-w",
    "--weights",
    action="extend",
    type=float,
    nargs="*",
    help=f"""
        The weights used for attention layers.
        There must be 12.
        default={WEIGHTS}
    """,
)

parser.add_argument(
    "-m",
    "--maxexamples",
    type=int,
    default=-1,
    help=f"""
        The maximum number of examples to process from the data set.
        Use a non-positive number to process all examples from the data set.
        default={-1}
    """,
)

parser.add_argument(
    "--examples",
    type=str,
    default=DEFAULT_EXAMPLES_FILE,
    help=f"""
        Specify the input file containing examples.
        default={DEFAULT_EXAMPLES_FILE}
    """,
)

parser.add_argument(
    "--output",
    default=DEFAULT_OUTPUT_FORMAT,
    help=f"""
        Specify the output format.
        Available formats: {OUTPUT_FORMATS}
        default={DEFAULT_OUTPUT_FORMAT}
    """,
)

if __name__ == '__main__':
    args = parser.parse_args()
    logging.basicConfig(
        filename=LOG_FILE,
        encoding="utf-8",
        filemode="w",
        level=getattr(logging, args.log.upper(), DEFAULT_LOG_LEVEL),
    )
    if args.output == "txt":
        print(f"Generating log file '{LOG_FILE}' in the project dir")
    csv.field_size_limit(sys.maxsize)

    weights = args.weights if args.weights else WEIGHTS[:]
    if len(weights) != 12:
        raise ValueError("If you specify weights, there must be exactly 12.")

    logger.info("Reading examples from %s", args.examples)
    try:
        with open(args.examples, "r", encoding="utf-8") as examples_file:
            data_arr = list(csv.reader(examples_file, delimiter=CSV_DELIMITER))
    except UnicodeDecodeError:
        logger.critical(
            "Dictionary file '%s' is not in utf-8 format",
            args.examples,
        )
        raise
    logger.info("Finished reading examples from file")

    dictionary = []
    preprocessed_arabic_list = []
    for row in data_arr:
        try:
            preprocessed_arabic = re.split(r"\[[A-Za-z-_]+\]", row[0])[0]
            preprocessed_arabic_list.append(preprocessed_arabic)
            english_example, arabic_example = get_examples(row[0])
            term_kwargs = {
                "word": row[1],
                "definition": row[3],
                "arabic_example": arabic_example,
                "english_example": english_example,
            }
            dictionary.append(DictionaryTerm(**term_kwargs))
        except ValueError:
            logger.warning("Bad user input for dictionary row: %s", row)
        except IndexError:
            logger.warning("Missing example in row: '%s'", row)

    logger.info(
        "Accepted input rows in user-defined dictionary: %s of %s",
        len(dictionary),
        len(data_arr),
    )

    criteria = (
        lambda term: term.is_target_recognized(),
        lambda term: term.is_word_in_example(),
    )
    logger.info(
        "Recognized %s terms out of %s",
        len([t.is_target_recognized() for t in dictionary]),
        len(dictionary),
    )
    logger.info(
        "Recognized terms in arabic examples: %s out of %s",
        len([t.is_word_in_example() for t in dictionary]),
        len(dictionary),
    )
    for term in dictionary:
        if term.is_target_recognized() and not term.is_word_in_example():
            logger.info(
                "Term %s is recognized but not in arabic example %s",
                term.word,
                term.arabic_example,
            )
            logger.info(
                "Example tokens: %s",
                arabic_string_to_tokens(term.arabic_example).split(" ")
            )

    dictionary = [
        term for term in dictionary
        if all(criterion(term) for criterion in criteria)
    ]
    if args.maxexamples > 0 and len(dictionary) > args.maxexamples:
        dictionary = dictionary[:args.maxexamples]

    ArabicTermResult.set_weights(weights)
    arabic_term_results = []
    n_top, n_top_three, n_top_half = 0, 0, 0
    for idx, term in enumerate(dictionary):
        if idx % 100 == 0 and args.output == "txt":
            print(f"Assessing example {idx+1} of {len(dictionary)}.")
        result_a = ArabicTermResult(
            term.arabic_example,
            preprocessed_arabic_list[idx],
            target_term=term.word,
        )
        if result_a.target_position == 0:
            n_top += 1
        if result_a.target_position >= 0 and result_a.target_position <= 2:
            n_top_three += 1
        if result_a.target_position >= 0 and result_a.target_position <= len(result_a.terms)/2.0:
            n_top_half += 1
        arabic_term_results.append(result_a)

    percent_top, percent_top_three, percent_top_half = 0.0, 0.0, 0.0
    if len(dictionary) > 0:
        percent = lambda n: 100.0*n/len(dictionary)
        percent_top = percent(n_top)
        percent_top_three = percent(n_top_three)
        percent_top_half = percent(n_top_half)

    if args.output == "txt":
        print("\n")
        print(dedent(f"""
            ================================================================================

                              Results for Arabic Terms, ignoring meaning

            ================================================================================

            Percent correct top spot: {percent_top}     
            Percent correct top three: {percent_top_three}
            Percent correct top half: {percent_top_half}

        """))

    results = []
    Result.set_dictionary(dictionary)
    for preliminary_result in arabic_term_results:
        dictionary_term = next((
            t for t in dictionary
            if arabic.equals(preliminary_result.arabic_example, t.arabic_example)
        ), None)
        if dictionary_term:
            try:
                results.append(Result(preliminary_result, dictionary_term.english_example))
            except ValueError as error:
                logger.warning(
                    "Failed to initialize result %s",
                    preliminary_result,
                )
        else:
            logger.warning(
                "Failed to find dictionary entry for preliminary result %s",
                preliminary_result,
            )

    if args.output == "txt":
        print("\n")
        for r in results:
            print(r.as_string())
            print("\n")
    elif args.output == "csv":
        print(Result.csv_header(CSV_DELIMITER))
        for r in results:
            print(r.as_csv_string(CSV_DELIMITER))
    elif args.output == "json":
        print(json.dumps(
            [dict(r) for r in results],
            indent=4,
            ensure_ascii=False,
        ))
    else:
        raise ValueError(f"Unknown output format '{args.output}'. See --help")
